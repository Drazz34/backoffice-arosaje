import {Conseil, Plante, Utilisateur} from "@/db/models";
import connection from "@/db/connection";

/**
 * Create a new advices.
 * @param data
 */
export async function createAdvice(data: any): Promise<ApiResponseType> {

    if (Object.keys(data).length == 0) {
        return {
            body: {
                message: `Le corps de la requête est vide.`
            },
            httpStatusCode: {
                status: 400
            }
        };
    }

    const plante = await Plante.findByPk(data.plante_id);
    const botaniste = await Utilisateur.findByPk(data.botaniste_id);

    if (!plante || !botaniste) {
        return {
            body: {
                message: `La plante ou le botaniste est introuvable.`
            },
            httpStatusCode: {
                status: 400
            }
        };
    }

    try {
        const advice = await connection.transaction(async (t: any) => {
            return await Conseil.create({
                contenu: data.contenu,
                date_creation: new Date(),
                date_modif: new Date(),
                plante_id: data.plante_id,
                botaniste_id: data.botaniste_id,
            }, {transaction: t});
        });

        return {
            body: {
                message: `Le conseil ${advice.id} a bien été créé.`,
                advice: advice,
            },
            httpStatusCode: {
                status: 201
            }
        };

    } catch (error) {
        return {
            body: {
                message: `Une erreur est survenue lors de l'enregistrement du conseil.`
            },
            httpStatusCode: {
                status: 500
            }
        };
    }
}
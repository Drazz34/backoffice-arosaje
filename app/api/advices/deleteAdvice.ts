import {Conseil} from "@/db/models";
import connection from "@/db/connection";

/**
 * Delete advices
 * @param advice_id
 */
export async function deleteAdvice(advice_id: number): Promise<ApiResponseType> {
    const advice = await Conseil.findByPk(advice_id);

    if (!advice) {
        return {
            body: {
                message: `Conseil introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    try {
        await connection.transaction(async (t: any) => {
            await advice.destroy({transaction: t});
        });

        return {
            body: {
                message: `Le conseil ${advice.id} a bien été supprimé.`,
                advice: advice,
            },
            httpStatusCode: {
                status: 200
            }
        };
    } catch (error) {
        return {
            body: {
                message: `Une erreur est survenue lors de la suppression du conseil.`
            },
            httpStatusCode: {
                status: 500
            }
        };
    }
}
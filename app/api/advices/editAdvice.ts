import {Conseil} from "@/db/models";
import connection from "@/db/connection";

/**
 * Edit advices
 * @param advice_id
 * @param data
 */
export async function editAdvice(advice_id: number, data: any): Promise<ApiResponseType> {
    if (Object.keys(data).length == 0) {
        return {
            body: {
                message: `Le corps de la requête est vide.`
            },
            httpStatusCode: {
                status: 400
            }
        };
    }

    const advice = await Conseil.findByPk(advice_id);

    if (!advice) {
        return {
            body: {
                message: `Conseil introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    try {
        await connection.transaction(async (t: any) => {
            await advice.update({
                contenu: data.contenu,
                date_modif: new Date()
            }, {transaction: t});
        });

        return {
            body: {
                message: `Le conseil ${advice.id} a bien été édité.`,
                advice: advice,
            },
            httpStatusCode: {
                status: 200
            }
        };
    } catch (error) {
        return {
            body: {
                message: `Une erreur est survenue lors de l'édition du conseil.`
            },
            httpStatusCode: {
                status: 500
            }
        };
    }
}
import {Conseil, Image, Plante, Utilisateur} from "@/db/models";

/**
 * Get advices by ID
 * @param id
 */
export async function getAdvice(id: number): Promise<ApiResponseType> {
    const advice = await Conseil.findByPk(
        id,
        {
            include: [
                {
                    model: Utilisateur,
                    as: 'Botaniste',
                    include: [
                        {
                            model: Image,
                        }
                    ]
                },
                {
                    model: Plante,
                }
            ]
        });

    if (!advice) {
        return {
            body: {
                message: `Conseil introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Conseil trouvé.`, advice: advice
        },
        httpStatusCode: {
            status: 200
        }
    };
}
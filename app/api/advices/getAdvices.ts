import {Conseil, Image, Plante, Utilisateur} from "@/db/models";

/**
 * Get all advices
 */
export async function getAdvices(): Promise<ApiResponseType> {
    const advices = await Conseil.findAll({
        order: [
            ["date_modif", "DESC"],
            ["date_creation", "DESC"]
        ],
        include: [
            {
                model: Utilisateur,
                as: 'Botaniste',
                include: [
                    {
                        model: Image,
                    }
                ]
            },
            {
                model: Plante,
            }
        ]
    });

    if (advices.length == 0) {
        return {
            body: {
                message: `Aucun conseils disponible.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Liste des conseils.`,
            advices: advices
        },
        httpStatusCode: {
            status: 200
        }
    };
}
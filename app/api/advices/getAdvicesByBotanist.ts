import {Conseil, Image, Plante, Utilisateur} from "@/db/models";

/**
 * Get all advices posted by a botanist.
 * @param botanist_id
 */
export async function getAdvicesByBotanist(botanist_id: number): Promise<ApiResponseType> {
    const botanist = await Utilisateur.findOne({
        where: {
            id: botanist_id,
            est_botaniste: true
        }
    });

    if (!botanist) {
        return {
            body: {
                message: `Botaniste introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    const conseils = await Conseil.findAll({
        where: {
            botaniste_id: botanist.id
        },
        order: [
            ["date_modif", "DESC"],
            ["date_creation", "DESC"]
        ],
        include: [
            {
                model: Utilisateur,
                as: 'Botaniste',
                include: [
                    {
                        model: Image,
                    }
                ]
            },
            {
                model: Plante,
            }
        ]
    });

    if (conseils.length == 0) {
        return {
            body: {
                message: `Ce botaniste n'a donné aucun conseil.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Conseils posté par ${botanist.pseudo}.`,
            conseils: conseils
        },
        httpStatusCode: {
            status: 200
        }
    };
}
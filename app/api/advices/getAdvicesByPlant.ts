import {Conseil, Image, Plante, Utilisateur} from "@/db/models";

export async function getAdvicesByPlant(plant_id: number) : Promise<ApiResponseType> {
    const plant = await Plante.findByPk(plant_id);

    if (!plant) {
        return {
            body: {
                message: `Plante introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    const conseils = await Conseil.findAll({
        where: {
            plante_id: plant.id
        },
        order: [
            ["date_modif", "DESC"],
            ["date_creation", "DESC"]
        ],
        include: [
            {
                model: Utilisateur,
                as: 'Botaniste',
                include: [
                    {
                        model: Image,
                    }
                ]
            },
            {
                model: Plante,
            }
        ]
    });

    if (!conseils) {
        return {
            body: {
                message: `Aucun conseils disponible pour cette plante.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Conseils pour la plante ${plant.nom_plante}.`,
            conseils: conseils
        },
        httpStatusCode: {
            status: 200
        }
    };
}
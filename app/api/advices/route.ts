export const dynamic = 'force-dynamic'
import {NextRequest, NextResponse} from "next/server";
import {getAdvice} from "@/app/api/advices/getAdvice";
import {getAdvices} from "@/app/api/advices/getAdvices";
import {createAdvice} from "@/app/api/advices/createAdvice";
import {editAdvice} from "@/app/api/advices/editAdvice";
import {deleteAdvice} from "@/app/api/advices/deleteAdvice";
import {getAdvicesByPlant} from "@/app/api/advices/getAdvicesByPlant";
import {getAdvicesByBotanist} from "@/app/api/advices/getAdvicesByBotanist";

/**
 * Endpoint returning all advices or a specific advices
 * @param req
 * @constructor
 */
export async function GET(
    req: NextRequest,
) : Promise<NextResponse> {
    const advice_id = req.nextUrl.searchParams.get('id');
    const botanist_id = req.nextUrl.searchParams.get('botanist');
    const plant_id = req.nextUrl.searchParams.get('plant');

    if (advice_id) {
        const response: ApiResponseType = await getAdvice(parseInt(advice_id));
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    if (botanist_id) {
        const response: ApiResponseType = await getAdvicesByBotanist(parseInt(botanist_id));
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    if (plant_id) {
        const response: ApiResponseType = await getAdvicesByPlant(parseInt(plant_id));
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const response: ApiResponseType = await getAdvices();
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to create an advices
 * @param req
 * @constructor
 */
export async function POST(req: NextRequest) {
    const data = await req.json();
    const response: ApiResponseType = await createAdvice(data);
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to edit an advices
 * @param req
 * @constructor
 */
export async function PUT(req: NextRequest) {
    let advice_id = req.nextUrl.searchParams.get('id');

    if (!advice_id) {
        const response: ApiResponseType = {
            body: {
                message: `La requête est invalide : paramètre manquant.`
            },
            httpStatusCode: {
                status: 400
            }
        };
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const data = await req.json();
    const response: ApiResponseType = await editAdvice(parseInt(advice_id), data);
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to delete an advices
 * @param req
 * @constructor
 */
export async function DELETE(req: NextRequest) {
    let advice_id = req.nextUrl.searchParams.get('id');

    if (!advice_id) {
        const response: ApiResponseType = {
            body: {
                message: `Le corps de la requête est vide.`
            },
            httpStatusCode: {
                status: 400
            }
        };
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const response: ApiResponseType = await deleteAdvice(parseInt(advice_id));
    return NextResponse.json(response.body, response.httpStatusCode);
}
import connection from "@/db/connection";
import {Annonce, Image, Plante} from "@/db/models";

/**
 * Create an announcements
 * @param data
 */
export async function createAnnouncement(data: any): Promise<ApiResponseType> {

    if (Object.keys(data).length == 0) {
        return {
            body: { message: `Le corps de la requête est vide.` },
            httpStatusCode: { status: 400 }
        };
    }

    try {
        const annonce = await connection.transaction(async (t: any) => {
            const plante = await Plante.findOrCreate({
                where: {
                    nom_plante: data.nom_plante
                },
                defaults: {
                    nom_plante: data.nom_plante,
                    description: data.description ? data.description : null
                }, transaction: t
            });

            const image = await Image.create({
                chemin: data.image
            }, {transaction: t});

            return await Annonce.create({
                date_debut: new Date(data.date_debut),
                date_fin: new Date(data.date_fin),
                periodicite_entretien: data.periodicite_entretien,
                date_creation: new Date(),
                date_modif: new Date(),
                image_id: image.id,
                plante_id: plante[0].id,
                proprietaire_id: data.proprietaire_id,
            }, {transaction: t});
        });

        return {
            body: {
                message: `L'annonce ${annonce.id} a bien été créé.`,
                annonce
            },
            httpStatusCode: { status: 201 }
        };

    } catch (error) {
        return {
            body: { message: `Une erreur est survenue lors de l'enregistrement de l'annonce.` },
            httpStatusCode: { status: 500 }
        };
    }
}

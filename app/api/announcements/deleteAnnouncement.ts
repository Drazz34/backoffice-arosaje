import connection from "@/db/connection";
import {Annonce} from "@/db/models";

/**
 * Delete an announcements
 * @param announcement_id
 */
export async function deleteAnnouncement(announcement_id: number) : Promise<ApiResponseType> {
    const annonce = await Annonce.findByPk(announcement_id);

    if (!annonce) {
        return {
            body: { message: `Annonce introuvable.` },
            httpStatusCode: { status: 404 }
        };
    }

    try {
        await connection.transaction(async (t: any) => {
            await annonce.destroy({transaction: t});
        });

        return {
            body: {
                message: `L'annonce ${annonce.id} a bien été supprimé.`,
                annonce: annonce,
            },
            httpStatusCode: { status: 200 }
        };
    } catch (error) {
        return {
            body: { message: `Une erreur est survenue lors de la suppression de l'annonce.` },
            httpStatusCode: { status: 500 }
        };
    }
}

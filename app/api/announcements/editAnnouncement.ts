import connection from "@/db/connection";
import {Annonce} from "@/db/models";

/**
 * Edit an announcements
 * @param announcement_id
 * @param data
 */
export async function editAnnouncement(announcement_id: number, data: any): Promise<ApiResponseType> {

    if (Object.keys(data).length == 0) {
        return {
            body: { message: `Le corps de la requête est vide.` },
            httpStatusCode: { status: 400 }
        };
    }

    const annonce = await Annonce.findByPk(announcement_id);

    if (!annonce) {
        return {
            body: { message: `Annonce introuvable.` },
            httpStatusCode: { status: 404 }
        };
    }

    try {
        await connection.transaction(async (t: any) => {
            await annonce.update({
                date_debut: data.date_debut ? data.date_debut : annonce.date_debut,
                date_fin: data.date_fin ? data.date_fin : annonce.date_fin,
                periodicite_entretien: data.periodicite_entretien ? data.periodicite_entretien : annonce.periodicite_entretien,
                gardien_id: data.gardien_id ? data.gardien_id : annonce.gardien_id,
                date_modif: new Date()
            }, {transaction: t});
        });

        return {
            body: {
                message: `L'annonce ${annonce.id} a bien été édité.`,
                annonce: annonce,
            },
            httpStatusCode: { status: 200 }
        };
    } catch (error) {
        return {
            body: {
                message: `Une erreur est survenue lors de l'édition de l'annonce.`
            },
            httpStatusCode: {
                status: 500
            }
        };
    }
}

import {Annonce, Entretien, Plante, Image, Utilisateur, Adresse, Ville, CodePostal} from "@/db/models";

/**
 * Get an announcements by its id
 * @param id
 */
export async function getAnnouncement(id: number): Promise<ApiResponseType> {
    const annonce = await Annonce.findByPk(
        id,
        {
            include: [
                {
                    model: Image,
                },
                {
                    model: Plante,
                },
                {
                    model: Utilisateur,
                    as: 'Proprietaire',
                    include: [
                        {
                            model: Adresse,
                            include: [
                                {
                                    model: Ville
                                },
                                {
                                    model: CodePostal
                                }
                            ]
                        }
                    ]
                },
                {
                    model: Utilisateur,
                    as: 'Gardien',
                },
                {
                    model: Entretien,
                    include: [
                        {
                            model: Image,
                        }
                    ]
                }
            ]
        }
    );

    if (!annonce) {
        return {
            body: {
                message: `Annonce introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Annonce trouvée.`,
            annonce: annonce
        },
        httpStatusCode: {
            status: 200
        }
    };
}

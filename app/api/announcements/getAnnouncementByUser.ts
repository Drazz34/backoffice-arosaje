import {Adresse, Annonce, CodePostal, Entretien, Image, Plante, Utilisateur, Ville} from "@/db/models";
import Sequelize from 'sequelize';

/**
 * Get an announcements by a users id
 * @param user_id
 */
export async function getAnnouncementByUser(user_id: number): Promise<ApiResponseType> {
    const user = await Utilisateur.findByPk(user_id);

    if (!user) {
        return {
            body: {
                message: `Utilisateur introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    const annonces = await Annonce.findAll({
        where: {
            [Sequelize.Op.or]: [
                {proprietaire_id: user.id},
                {gardien_id: user.id}
            ]
        },
        include: [
            {
                model: Image,
            },
            {
                model: Plante,
            },
            {
                model: Utilisateur,
                as: 'Proprietaire',
                include: [
                    {
                        model: Adresse,
                        include: [
                            {
                                model: Ville
                            },
                            {
                                model: CodePostal
                            }
                        ]
                    }
                ]
            },
            {
                model: Utilisateur,
                as: 'Gardien',
            },
            {
                model: Entretien,
                include: [
                    {
                        model: Image,
                    }
                ]
            }
        ]
    });

    if (annonces.length == 0) {
        return {
            body: {
                message: `Annonces introuvables.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Liste des annonces posté par ${user.pseudo}.`,
            annonces: annonces
        },
        httpStatusCode: {
            status: 200
        }
    };
}

import {Adresse, Annonce, CodePostal, Entretien, Image, Plante, Utilisateur, Ville} from "@/db/models";

/**
 * Get all announcements
 */
export async function getAnnouncements(): Promise<ApiResponseType> {
    const annonces = await Annonce.findAll({
        order: [["date_debut", "ASC"]],
        include: [
            {
                model: Image,
            },
            {
                model: Plante,
            },
            {
                model: Utilisateur,
                as: 'Proprietaire',
                include: [
                    {
                        model: Adresse,
                        include: [
                            {
                                model: Ville
                            },
                            {
                                model: CodePostal
                            }
                        ]
                    }
                ]
            },
            {
                model: Utilisateur,
                as: 'Gardien',
            },
            {
                model: Entretien,
                include: [
                    {
                        model: Image,
                    }
                ]
            }
        ]
    });

    if (annonces.length == 0) {
        return {
            body: {
                message: `Annonce introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Liste des annonces.`,
            annonces: annonces
        },
        httpStatusCode: {
            status: 200
        }
    };
}

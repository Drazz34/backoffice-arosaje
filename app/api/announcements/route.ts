import {editAnnouncement} from "@/app/api/announcements/editAnnouncement";

export const dynamic = 'force-dynamic'
import {NextRequest, NextResponse} from "next/server";
import {createAnnouncement} from "@/app/api/announcements/createAnnouncement";
import {deleteAnnouncement} from "@/app/api/announcements/deleteAnnouncement";
import {getAnnouncement} from "@/app/api/announcements/getAnnouncement";
import {getAnnouncementByUser} from "@/app/api/announcements/getAnnouncementByUser";
import {getAnnouncements} from "@/app/api/announcements/getAnnouncements";

/**
 * Endpoint returning all announcements or a specific announcements
 * @param req
 * @constructor
 */
export async function GET(
    req: NextRequest,
) : Promise<NextResponse> {
    const announcement_id = req.nextUrl.searchParams.get('id');
    const user_id = req.nextUrl.searchParams.get('user');
    console.log(announcement_id);

    if (announcement_id) {
        const response: ApiResponseType = await getAnnouncement(parseInt(announcement_id));
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    if (user_id) {
        const response: ApiResponseType = await getAnnouncementByUser(parseInt(user_id));
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const response: ApiResponseType = await getAnnouncements();
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to create an announcements
 * @param req
 * @constructor
 */
export async function POST(req: NextRequest) {
    const data = await req.json();
    const response: ApiResponseType = await createAnnouncement(data);
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to edit an announcements
 * @param req
 * @constructor
 */
export async function PUT(req: NextRequest) {
    let announcement_id = req.nextUrl.searchParams.get('id');

    if (!announcement_id) {
        return NextResponse.json(
            { message: `La requête est invalide : paramètre manquant.` },
            { status: 400 }
        );
    }

    const data = await req.json();
    const response: ApiResponseType = await editAnnouncement(parseInt(announcement_id), data);
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to delete an announcements
 * @param req
 * @constructor
 */
export async function DELETE(req: NextRequest) {
    let announcement_id = req.nextUrl.searchParams.get('id');

    if (!announcement_id) {
        return NextResponse.json(
            { message: `Le corps de la requête est vide.` },
            { status: 400 }
        );
    }

    const response: ApiResponseType = await deleteAnnouncement(parseInt(announcement_id));
    return NextResponse.json(response.body, response.httpStatusCode);
}
import {NextRequest, NextResponse} from "next/server";
import {RefreshToken} from "@/db/models";
import {logout} from "@/app/lib/auth/auth";

export const dynamic = 'force-dynamic'
export interface I_ApiUserRefreshTokenResponse extends ApiResponse {}

export async function GET(request: NextRequest) {
    const refreshToken = request.cookies.get('refreshToken')?.value;

    if (refreshToken) {
        const refreshTokenEntity = await RefreshToken.destroy({
            where: {
                token: refreshToken
            }
        });
    }

    logout();
    // create our response object
    const res: I_ApiUserRefreshTokenResponse = {
        success: true,
    };
    return NextResponse.json(res, {status: 200});
}
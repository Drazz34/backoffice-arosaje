import {NextRequest, NextResponse} from "next/server";
import {RefreshToken, Utilisateur} from "@/db/models";
import {
    generateAccessToken,
    verifyJwtRefreshToken,
    logout,
    clearRefreshTokenCookie, setUserDataCookie, generateRefreshToken, setRefreshTokenCookie
} from "@/app/lib/auth/auth";

export interface I_ApiUserRefreshTokenResponse extends ApiResponse {}

export const dynamic = 'force-dynamic'
export async function GET(request: NextRequest) {
    let refreshToken = request.cookies.get('refreshToken')?.value;
    if (!refreshToken) {
        refreshToken = request.headers.get('Refresh-Token')?.toString();
    }
    clearRefreshTokenCookie();

    const response: ApiResponse = {
        success: false,
        message: 'Non autorisé',
    };
    const unauthorizedResponse = NextResponse.json(response, { status: 401 });

    if (!refreshToken) {
        logout();
        return unauthorizedResponse;
    }

    const user = await Utilisateur.findOne({
        include: {
            model: RefreshToken,
            where: {
                token: refreshToken
            }
        }
    });

    // Detected refresh token reuse !
    if (!user) {
        const payload = await verifyJwtRefreshToken(refreshToken);
        if (payload) {
            try {
                await RefreshToken.destroy({
                    where: {
                        utilisateur_id: payload.id
                    }
                })
            } catch (e) {
                return NextResponse.json("Une erreur est survenue", {status: 500});
            }
        }
        logout();
        return unauthorizedResponse;
    }

    const payload = await verifyJwtRefreshToken(refreshToken);

    if (!payload) {
        await RefreshToken.destroy({
            where: {
                token: refreshToken
            }
        });
        logout();
        return unauthorizedResponse;
    }

    if (user.id !== payload.id) {
        logout();
        return unauthorizedResponse;
    }

    // RefreshToken is still valid, so we generate new access token
    const accessToken = await generateAccessToken(user);

    // Delete old refresh token
    await RefreshToken.destroy({
        where: {
            token: refreshToken
        }
    });

    // Generate new refresh token
    const newRefreshToken = await generateRefreshToken(user);

    // Add the new refresh token in database
    await RefreshToken.create({
        token: newRefreshToken,
        utilisateur_id: user.id
    });

    // Set new refresh token in cookies
    setRefreshTokenCookie(newRefreshToken);

    // If users data change in DB, set new users data in cookie
    setUserDataCookie(user.exportPublic());

    // create our response object
    const res: I_ApiUserRefreshTokenResponse = {
        success: true,
        token: accessToken,
        refreshToken: newRefreshToken,
    };

    return NextResponse.json(res, { status: 200 });
}
import { NextResponse, NextRequest } from 'next/server';
import { Utilisateur, RefreshToken } from '@/db/models';

import {
	setUserDataCookie,
	generateAccessToken,
	generateRefreshToken, setRefreshTokenCookie
} from '@/app/lib/auth/auth';
import authConfig from '@/config/authConfig';

export interface I_ApiUserLoginRequest {
	login: string;
	password: string;
	tsToken: string;
	code?: string;
}

export interface I_ApiUserLoginResponse extends ApiResponse {}

export const dynamic = 'force-dynamic'
export async function POST(request: NextRequest) {
	const refreshToken = request.cookies.get('refreshToken')?.value ? request.cookies.get('refreshToken')?.value : '';
	const body = (await request.json()) as I_ApiUserLoginRequest;

	// trim all input values
	const { login, password, tsToken } = Object.fromEntries(
		Object.entries(body).map(([key, value]) => [key, value?.trim()]),
	) as I_ApiUserLoginRequest;

	if (!login || !password) {
		const res: I_ApiUserLoginResponse = {
			success: false,
			message: "L'identifiant et le mot de passe sont requis.",
		};

		return NextResponse.json(res, { status: 400 });
	}

	try {
		// Fetch our users from the database
		const user = await Utilisateur.login(login, password);

		if (!user) {
			const res: I_ApiUserLoginResponse = {
				success: false,
				message: 'Identifiant ou mot de passe incorrect.',
			};

			return NextResponse.json(res, { status: 401 });
		}

		// Create and sign our JWT
		const accessToken = await generateAccessToken(user);

		// Create and sign our refresh token
		const newRefreshToken = await generateRefreshToken(user);

		/*
		Scenario added here :
			1 - User logs but never uses refresh token and does not log out
			2 - refresh token is stolen and used by someone else
			3 - If 1 & 2, reuse detection is needed to clear all refresh tokens when users logs in
		*/

		const userWithOldToken = await Utilisateur.findOne({
			include: {
				model: RefreshToken,
				where: {
					token: refreshToken
				}
			}
		});

		// Detected refresh token reuse !
		if (!userWithOldToken) {
			await RefreshToken.destroy({
				where: {
					utilisateur_id: user.id
				}
			});
		}

		// Delete old refresh token in database
		if (refreshToken) {
			await RefreshToken.destroy({
				where: {
					token: refreshToken
				}
			});
		}

		// Adding new refresh token in database
		await RefreshToken.create({
			token: newRefreshToken,
			utilisateur_id: user.id,
		});

		// create our response object
		const res: I_ApiUserLoginResponse = {
			success: true,
			token: accessToken,
			refreshToken: newRefreshToken,
		};

		const response = NextResponse.json(res, { status: 200 });

		setRefreshTokenCookie(newRefreshToken);

		// Store public users data as a cookie
		const userData = user.exportPublic();
		setUserDataCookie(userData);

		return response;
	} catch (error: any) {
		console.log(error);

		const res: I_ApiUserLoginResponse = {
			success: false,
			message: error.message || 'Une erreur est survenue.',
		};

		return NextResponse.json(res, { status: 500 });
	}
}
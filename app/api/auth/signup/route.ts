import {NextRequest, NextResponse} from "next/server";
import {
    Utilisateur,
    Genre,
    Image,
    Adresse,
    Ville,
    CodePostal,
} from "@/db/models";
import connection from "@/db/connection";
import authConfig from "@/config/authConfig";

const bcrypt = require("bcrypt");

export const dynamic = 'force-dynamic'
export async function POST(req: NextRequest) {
    const data = await req.json();

    if (Object.keys(data).length == 0) {
        return NextResponse.json(
            {
                message: `Le corps de la requête est vide.`
            },
            {status: 400}
        );
    }

    const genre = await Genre.findByPk(data.genre);

    if (!genre) {
        return NextResponse.json(
            {
                message: `Genre introuvable.`
            },
            {status: 404,}
        );
    }

    try {
        const user = await connection.transaction(async (t: any) => {
            const image = await Image.create({
                chemin: data.image,
            });

            const ville = await Ville.findOrCreate({
                where: {
                    nom_ville: data.ville,
                },
                defaults: {
                    nom_ville: data.ville,
                }
            });

            const codePostal = await CodePostal.findOrCreate({
                where: {
                    code_postal: data.code_postal,
                },
                defaults: {
                    code_postal: data.code_postal,
                }
            });

            const adresse = await Adresse.findOrCreate({
                where: {
                    rue: null,
                    ville_id: ville[0].id,
                    code_postal_id: codePostal[0].id,
                },
                defaults: {
                    rue: null,
                    ville_id: ville[0].id,
                    code_postal_id: codePostal[0].id,
                }
            });

            const hashedPassword = await bcrypt.hash(data.mot_de_passe, authConfig.saltRounds);

            return await Utilisateur.create({
                pseudo: data.pseudo,
                mail: data.mail,
                mot_de_passe: hashedPassword,
                est_botaniste: data.est_botaniste,
                est_admin: data.est_admin,
                date_creation: new Date(),
                genre_id: genre.id,
                image_id: image.id,
                adresse_id: adresse[0].id,
            });
        });

        delete user.dataValues.mot_de_passe;

        return NextResponse.json(
            {
                message: "L'utilisateur a bien été ajouté.",
                user: user,
            },
            {status: 200}
        );
    } catch (error) {
        return NextResponse.json(
            {message: `Une erreur est survenue lors de l'ajout de l'utilisateur.`},
            {status: 500}
        );
    }
}

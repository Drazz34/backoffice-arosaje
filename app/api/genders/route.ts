import {Annonce, Entretien, Genre, Image, Plante, Utilisateur} from "@/db/models";
import {NextRequest, NextResponse} from "next/server";

export const dynamic = 'force-dynamic'
export async function GET(req: NextRequest) {
    const genres = await Genre.findAll({});

    if (genres.length == 0) {
        return NextResponse.json(
            {message: `Annonce introuvable.`},
            {status: 404}
        );
    }

    return NextResponse.json(
        genres,
        {status: 200}
    );
}

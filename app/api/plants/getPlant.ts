import {Plante, Image, Utilisateur, Conseil} from "@/db/models";

/**
 * Get a plant by its id.
 * @param id
 */
export async function getPlant(id: number): Promise<ApiResponseType> {
    const plant = await Plante.findByPk(
        id,
        {
        include: [
            {
                model: Conseil,
                include: [
                    {
                        model: Utilisateur,
                        as: 'Botaniste',
                        include: [
                            {
                                model: Image
                            }
                        ]
                    }
                ]
            }
        ]
    });

    if (!plant) {
        return {
            body: {
                message: `Plante introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Plante trouvée.`,
            annonce: plant
        },
        httpStatusCode: {
            status: 200
        }
    };
}

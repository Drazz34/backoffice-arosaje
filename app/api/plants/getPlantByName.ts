import {Plante, Image, Utilisateur, Conseil} from "@/db/models";
import Sequelize from "sequelize";

/**
 * Get a plant by its name.
 * @param name
 */
export async function getPlantByName(name: string): Promise<ApiResponseType> {
    const plant = await Plante.findAll({
        where: {
            nom_plante: {
                [Sequelize.Op.like]: `%${name}%`
            }

        },
        include: [
            {
                model: Conseil,
                include: [
                    {
                        model: Utilisateur,
                        as: 'Botaniste',
                        include: [
                            {
                                model: Image
                            }
                        ]
                    }
                ]
            }
        ]
    });

    if (!plant) {
        return {
            body: {
                message: `Plante introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Plante trouvée.`,
            annonce: plant
        },
        httpStatusCode: {
            status: 200
        }
    };
}

import {Plante, Image, Utilisateur, Conseil} from "@/db/models";

/**
 * Get all plants.
 */
export async function getPlants(): Promise<ApiResponseType> {
    const plants = await Plante.findAll({
        include: [
            {
                model: Conseil,
                include: [
                    {
                        model: Utilisateur,
                        as: 'Botaniste',
                        include: [
                            {
                                model: Image
                            }
                        ]
                    }
                ]
            }
        ]
        });

    if (plants.length == 0) {
        return {
            body: {
                message: `Plante introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Liste des plantes.`,
            annonces: plants
        },
        httpStatusCode: {
            status: 200
        }
    };
}

import {NextRequest, NextResponse} from "next/server";
import {getPlant} from "@/app/api/plants/getPlant";
import {getPlantByName} from "@/app/api/plants/getPlantByName";
import {getPlants} from "@/app/api/plants/getPlants";

export const dynamic = 'force-dynamic'

/**
 * Endpoint returning all plants or a specific plant
 * @param req
 * @constructor
 */
export async function GET(req: NextRequest) {
    const plant_id = req.nextUrl.searchParams.get('id');
    const name = req.nextUrl.searchParams.get('name');

    if (plant_id) {
        const response: ApiResponseType = await getPlant(parseInt(plant_id));
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    if (name) {
        const response: ApiResponseType = await getPlantByName(name);
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const response: ApiResponseType = await getPlants();
    return NextResponse.json(response.body, response.httpStatusCode);
}
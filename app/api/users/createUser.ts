import {Utilisateur, Genre, Image, Adresse, Ville, CodePostal} from "@/db/models";
import connection from "@/db/connection";
import authConfig from "@/config/authConfig";
const bcrypt = require("bcrypt");

/**
 * Create a users
 * @param data
 */
export async function createUser(data: any): Promise<ApiResponseType> {

    if (Object.keys(data).length == 0) {
        return {
            body: {
                message: `Le corps de la requête est vide.`
            },
            httpStatusCode: {
                status: 400
            }
        };
    }

    const genre = await Genre.findByPk(data.genre);

    if (!genre) {
        return {
            body: {
                message: `Genre introuvable.`
            },
            httpStatusCode: {
                status: 404,
            }
        };
    }

    try {
        const user = await connection.transaction(async (t: any) => {
            const image = await Image.create({
                chemin: data.image,
            });

            const ville = await Ville.findOrCreate({
                where: {
                    nom_ville: data.ville,
                },
                defaults: {
                    nom_ville: data.ville,
                }
            });

            const codePostal = await CodePostal.findOrCreate({
                where: {
                    code_postal: data.code_postal,
                },
                defaults: {
                    code_postal: data.code_postal,
                }
            });

            const adresse = await Adresse.findOrCreate({
                where: {
                    rue: data.rue,
                    ville_id: ville[0].id,
                    code_postal_id: codePostal[0].id,
                },
                defaults: {
                    rue: data.rue,
                    ville_id: ville[0].id,
                    code_postal_id: codePostal[0].id,
                }
            });

            const hashedPassword = await bcrypt.hash(data.mot_de_passe, authConfig.saltRounds);

            return await Utilisateur.create({
                pseudo: data.pseudo,
                mail: data.mail,
                mot_de_passe: hashedPassword,
                est_botaniste: data.est_botaniste,
                est_admin: data.est_admin,
                date_creation: new Date(),
                genre_id: genre.id,
                image_id: image.id,
                adresse_id: adresse[0].id,
            });
        });

        delete user.dataValues.mot_de_passe;

        return {
            body: {
                message: "L'utilisateur a bien été créé.",
                user: user,
            },
            httpStatusCode: {
                    status: 201
                }
        };
    } catch (error) {
        return {
            body:
                {
                message: `Une erreur est survenue lors de l'ajout de l'utilisateur.`
            },
            httpStatusCode: {
                status: 500
            }
        };
    }
}

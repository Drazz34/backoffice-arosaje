import connection from "@/db/connection";
import {Utilisateur} from "@/db/models";

/**
 * Delete a users
 * @param user_id
 */
export async function deleteUser(user_id: number): Promise<ApiResponseType> {
    const user = await Utilisateur.findByPk(user_id);

    if (!user) {
        return {
            body: {
                message: `Utilisateur introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    try {
        await connection.transaction(async (t: any) => {
            await user.destroy({transaction: t});
        });

        return {
            body: {
                message: `Le utilisateur ${user.id} a bien été supprimé.`,
                user: user,
            },
            httpStatusCode: {
                status: 200
            }
        };
    } catch (error) {
        return {
            body: {
                message: `Une erreur est survenue lors de la suppression de l'utilisateur.`
            },
            httpStatusCode: {
                status: 500
            }
        };
    }
}

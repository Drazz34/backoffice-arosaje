import {Adresse, CodePostal, Utilisateur, Ville, Image} from "@/db/models";
import connection from "@/db/connection";
import * as http from "node:http";

/**
 * Edit a users
 * @param user_id
 * @param data
 */
export async function editUser(user_id: number, data: any): Promise<ApiResponseType> {
    const user = await Utilisateur.findByPk(
        user_id,
        {
            include: [
                {
                    model: Adresse,
                    include: [
                        {
                            model: Ville
                        },
                        {
                            model: CodePostal
                        }
                    ]
                },
                {
                    model: Image
                }
            ]
        }
    );

    if (!user) {
        return {
            body: {
                message: `Utilisateur introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    try {
        const ville = await Ville.findOrCreate({
            where: {
                nom_ville: data.ville ? data.ville : user.Adresse.Ville.nom_ville,
            },
            defaults: {
                nom_ville: data.ville,
            }
        });

        const codePostal = await CodePostal.findOrCreate({
            where: {
                code_postal: data.code_postal ? data.code_postal : user.Adresse.CodePostal.code_postal,
            },
            defaults: {
                code_postal: data.code_postal,
            }
        });


        const adresse = await Adresse.findOrCreate({
            where: {
                rue: data.rue ? data.rue : user.Adresse.rue,
                ville_id: ville[0].id,
                code_postal_id: codePostal[0].id,
            },
            defaults: {
                rue: data.rue,
                ville_id: ville[0].id,
                code_postal_id: codePostal[0].id,
            }
        });

        await connection.transaction(async (t: any) => {
            await user.update({
                pseudo: data.pseudo ? data.pseudo : user.pseudo,
                mail: data.mail ? data.mail : user.mail,
                est_botaniste: data.est_botaniste ? data.est_botaniste : user.est_botaniste,
                est_admin: data.est_admin ? data.est_admin : user.est_admin,
                date_modif: new Date(),
                genre_id: data.genre ? data.genre : user.genre_id,
                adresse_id: adresse[0].id,
            }, {transaction: t});

            await user.Image.update({
                chemin: data.image ? data.image : user.Image.chemin,
            }, {transaction: t});
        });

        const userUpdated = await Utilisateur.findByPk(
            user_id,
            {
                include: [
                    {
                        model: Adresse,
                        include: [
                            {
                                model: Ville
                            },
                            {
                                model: CodePostal
                            }
                        ]
                    },
                    {
                        model: Image
                    }
                ]
            }
        );

        return {
            body: {
                message: `Modification(s) validé(s).`,
                user: userUpdated,
            },
            httpStatusCode: {
                status: 200
            }
        };
    } catch (error) {
        console.log(error)
        return {
            body: {
                message: `Une erreur est survenue lors de l'édition de l'utilisateur.`
            },
            httpStatusCode: {
                status: 500
            }
        };
    }
}

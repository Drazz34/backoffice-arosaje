import {Adresse, CodePostal, Genre, Image, Utilisateur, Ville} from "@/db/models";

/**
 * Delete a users
 * @param id
 */
export async function getUser(id: number): Promise<ApiResponseType> {
    const user = await Utilisateur.findByPk(
        id,
        {
            include: [
                {
                    model: Genre
                },
                {
                    model: Image
                },
                {
                    model: Adresse,
                    include: [
                        {
                            model: Ville
                        },
                        {
                            model: CodePostal
                        }
                    ]
                }
            ]
        });

    if (!user) {
        return {
            body: {
                message: `Utilisateur introuvable.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Utilisateur trouvé.`,
            user: user
        },
        httpStatusCode: {
            status: 200
        }
    };
}

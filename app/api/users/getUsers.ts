import {Adresse, CodePostal, Image, Utilisateur, Ville} from "@/db/models";

/**
 * Get all users
 */
export async function getUsers(): Promise<ApiResponseType>{
    const users = await Utilisateur.findAll({
        order: [
            ["id", "DESC"]
        ],
        include: [
            {
                model: Adresse,
                include: [
                    {
                        model: Ville
                    },
                    {
                        model: CodePostal
                    }
                ]
            },
            {
                model: Image
            }
        ]
    });

    if (users.length == 0) {
        return {
            body: {
                message: `Aucun utilisateurs disponible.`
            },
            httpStatusCode: {
                status: 404
            }
        };
    }

    return {
        body: {
            message: `Liste des utilisateurs.`,
            users: users
        },
        httpStatusCode: {
            status: 200
        }
    };
}
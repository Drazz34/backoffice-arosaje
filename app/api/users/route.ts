import {NextRequest, NextResponse} from "next/server";
import {createUser} from "@/app/api/users/createUser";
import {editUser} from "@/app/api/users/editUser";
import {deleteUser} from "@/app/api/users/deleteUser";
import {getUser} from "@/app/api/users/getUser";
import {getUsers} from "@/app/api/users/getUsers";

export const dynamic = 'force-dynamic'

/**
 * Endpoint returning all advices or a specific advices
 * @param req
 * @constructor
 */
export async function GET(
    req: NextRequest,
) : Promise<NextResponse> {
    const user_id = req.nextUrl.searchParams.get('id');

    if (user_id) {
        const response: ApiResponseType = await getUser(parseInt(user_id));
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const response: ApiResponseType = await getUsers();
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to create an advices
 * @param req
 * @constructor
 */
export async function POST(req: NextRequest) {
    const data = await req.json();
    const response: ApiResponseType = await createUser(data);
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to edit an advices
 * @param req
 * @constructor
 */
export async function PUT(req: NextRequest) {
    let advice_id = req.nextUrl.searchParams.get('id');

    if (!advice_id) {
        const response: ApiResponseType = {
            body: {
                message: `La requête est invalide : paramètre manquant.`
            },
            httpStatusCode: {
                status: 400
            }
        };
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const data = await req.json();
    const response: ApiResponseType = await editUser(parseInt(advice_id), data);
    return NextResponse.json(response.body, response.httpStatusCode);
}

/**
 * Endpoint to delete an advices
 * @param req
 * @constructor
 */
export async function DELETE(req: NextRequest) {
    let advice_id = req.nextUrl.searchParams.get('id');

    if (!advice_id) {
        const response: ApiResponseType = {
            body: {
                message: `Le corps de la requête est vide.`
            },
            httpStatusCode: {
                status: 400
            }
        };
        return NextResponse.json(response.body, response.httpStatusCode);
    }

    const response: ApiResponseType = await deleteUser(parseInt(advice_id));
    return NextResponse.json(response.body, response.httpStatusCode);
}
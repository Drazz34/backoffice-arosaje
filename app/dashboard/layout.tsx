import type { Metadata } from 'next'
import SideNav from '../ui/dashboard/sidenav'

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <main className='flex p-4 min-h-dvh'>
      <SideNav />
      <div className="flex flex-col items-center gap-20 w-full p-4">
        {children}
      </div>
    </main>
  )
}
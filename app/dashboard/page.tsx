import AnnonceCard from "../ui/annonces/AnnonceCard";
import { quicksand } from "../ui/fonts";
import HeaderCard from "../ui/dashboard/header-card";
import {Annonce, Conseil, Plante, Utilisateur} from "@/db/models";
import {fetchAllAnnonces, fetchDashboardAnnonces} from "@/app/lib/fetch/annonces";

//TODO: force dynamic à enlever après avoir placé les call à la db dans un fichier de librairie
export const dynamic = 'force-dynamic'
export default async function Page() {

    const announcementHeaderCardProps = {
        content: "Annonces",
        count: await Annonce.count(),
    }

    const adviceHeaderCardProps = {
        content: "Conseils",
        count: await Conseil.count(),
    }

    const plantHeaderCardProps = {
        content: "Plantes",
        count: await Plante.count(),
    }

    const userHeaderCardProps = {
        content: "Utilisateurs",
        count: await Utilisateur.count(),
    }

    const botanistHeaderCardProps = {
        content: "Botanistes",
        count: await Utilisateur.count({
            where: {
                est_botaniste: true
            }
        }),
    }

    const annonces = await fetchDashboardAnnonces();

  return (
      <div className="w-full">
          <div className="w-full flex justify-between">
              <HeaderCard {...announcementHeaderCardProps}></HeaderCard>
              <HeaderCard {...adviceHeaderCardProps}></HeaderCard>
              <HeaderCard {...plantHeaderCardProps}></HeaderCard>
              <HeaderCard {...userHeaderCardProps}></HeaderCard>
              <HeaderCard {...botanistHeaderCardProps}></HeaderCard>
          </div>
          <div className="w-full">
              <div>
                  {
                      annonces.map(annonce => {
                          return (
                              <AnnonceCard key={annonce.id} {...annonce}></AnnonceCard>
                          )
                      })
                  }
              </div>
              <div>
              </div>
          </div>
          {/* <AnnonceCard></AnnonceCard> */}
      </div>
  )
}
import {poppins} from "@/app/ui/fonts";
import UsersTable from "@/app/ui/users/usersTable";
import {fetchUsersPage} from "@/app/lib/fetch/users";
import { Suspense } from 'react';
import Pagination from "@/app/ui/users/pagination";
import Search from "@/app/ui/search";
import {TableSkeleton} from "@/app/ui/skeletons";

export default async function Page({
  searchParams,
}: {
  searchParams?: {
    query?: string;
    page?: string;
  };
}) {
  const query = searchParams?.query || '';
  const currentPage = Number(searchParams?.page) || 1;
  const totalPages = await fetchUsersPage(query);
  return (
      <div className="w-full">
        <div className="mt-4 flex items-center justify-between gap-2 md:mt-8">
            <h1 className={`${poppins.className} text-2xl`}>Utilisateurs</h1>
            <Search placeholder="Search invoices..." />
            {/*<CreateInvoice />*/}
        </div>
        <Suspense key={query + currentPage} fallback={<TableSkeleton />}>
          <UsersTable query={query} currentPage={currentPage} />
        </Suspense>
        <div className="mt-5 flex w-full justify-center">
          <Pagination totalPages={totalPages} />
        </div>
      </div>
  );
}
import {JWTPayload, jwtVerify, SignJWT} from 'jose';
import {cookies} from 'next/headers';
import {UtilisateurVM} from '@/app/lib/view_models/UtilisateurVM';
import authConfig from "@/config/authConfig";

export function getJwtSecretKey() {
	const secret = process.env.ACCESS_TOKEN_SECRET;

	if (!secret) {
		throw new Error('JWT Secret key is not set');
	}

	return new TextEncoder().encode(secret);
}

export function getRefreshSecretKey() {
	const secret = process.env.REFRESH_TOKEN_SECRET;

	if (!secret) {
		throw new Error('JWT Secret key is not set');
	}

	return new TextEncoder().encode(secret);
}

export async function verifyJwtAccessToken(token: string): Promise<JWTPayload | null> {
	try {
		const { payload } = await jwtVerify(token, getJwtSecretKey());

		return payload;
	} catch (error) {
		return null;
	}
}

export async function verifyJwtRefreshToken(token: string): Promise<JWTPayload | null> {
	try {
		const { payload } = await jwtVerify(token, getRefreshSecretKey());

		return payload;
	} catch (error) {
		return null;
	}
}

export async function getJwt() {
	const cookieStore = cookies();
	const token = cookieStore.get('token');

	if (token) {
		try {
			const payload = await verifyJwtAccessToken(token.value);
			if (payload) {
				const authPayload: AuthPayload = {
					id: payload.id as string,
					pseudo: payload.pseudo as string,
					mail: payload.mail as string,
					est_botaniste: payload.est_botaniste as boolean,
					est_admin: payload.est_admin as boolean,
					iat: payload.iat as number,
					exp: payload.exp as number,
					openIdSub: payload.openIdSub as string,
				};
				return authPayload;
			}
		} catch (error) {
			return null;
		}
	}
	return null;
}

export function logout() {
	const cookieStore = cookies();

	const refreshToken = cookieStore.get('refreshToken');
	if (refreshToken) {
		try {
			cookieStore.delete('refreshToken');
		} catch (_) {}
	}

	const userData = cookieStore.get('userData');
	if (userData) {
		try {
			cookieStore.delete('userData');
			return true;
		} catch (_) {}
	}

	return null;
}

export function setUserDataCookie(userData: UtilisateurVM) {
	const cookieStore = cookies();

	cookieStore.set({
		name: 'userData',
		value: JSON.stringify(userData),
		path: '/',
		maxAge: 86400, // 24 hours
		sameSite: 'strict',
		secure: true,
	});
}

export function clearUserDataCookie() {
	const cookieStore = cookies();

	cookieStore.set({
		name: 'userData',
		value: '',
		path: '/',
		maxAge: 86400, // 24 hours
		sameSite: 'strict',
		secure: true,
	});
}

export function setRefreshTokenCookie(refreshToken: string) {
	const cookieStore = cookies();

	cookieStore.set({
		name: 'refreshToken',
		value: refreshToken,
		path: '/',
		maxAge: 86400, // 24 hours
		sameSite: 'strict',
		secure: true,
	});
}

export function clearRefreshTokenCookie() {
	const cookieStore = cookies();
	cookieStore.set({
		name: 'refreshToken',
		value: '',
		path: '/',
		maxAge: 86400, // 24 hours
		sameSite: 'strict',
		secure: true,
	});
}

export async function generateAccessToken(user: any) {
	return await new SignJWT({
		id: user.id,
		pseudo: user.pseudo,
		email: user.mail,
		est_admin: user.est_admin,
		est_botaniste: user.est_botaniste,
	})
		.setProtectedHeader({ alg: 'HS256' })
		.setIssuedAt()
		.setExpirationTime(`${authConfig.jwtExpires}s`)
		.sign(getJwtSecretKey());
}

export async function generateRefreshToken(user: any) {
	return await new SignJWT({
		id: user.id,
		pseudo: user.pseudo,
		email: user.mail,
		est_admin: user.est_admin,
		est_botaniste: user.est_botaniste,
	})
		.setProtectedHeader({ alg: 'HS256' })
		.setIssuedAt()
		.setExpirationTime(`${authConfig.refreshTokenExpires}s`)
		.sign(getRefreshSecretKey());
}
export const advices = {
    post: {
        summary: 'Ajouter un nouveau conseil',
        description: 'Permet d\'ajouter un nouveau conseil à la base de données.',
        tags: ['Conseils'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            contenu: {
                                type: 'string',
                                description: 'Le contenu du conseil.'
                            },
                            plante_id: {
                                type: 'integer',
                                description: "L'ID de la plante associée au conseil."
                            },
                            botaniste_id: {
                                type: 'integer',
                                description: "L'ID du botaniste qui a créé le conseil."
                            }
                        },
                        required: ['contenu', 'plante_id', 'botaniste_id']
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - Le conseil a été ajouté avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                },
                                conseil: {
                                    type: 'object',
                                    description: 'Détails du conseil ajouté.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '401': {
                description: 'Non autorisé.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de l\'ajout du conseil.'
            }
        }
    },
    get: {
        summary: 'Obtenir un ou plusieurs conseils',
        description:
            '<b>Sans param</b> - Récupère tous les conseils de la base de données.</br>'+
            '<b>Param id</b> - Récupère un conseil de la base de données par son ID.</br>' +
            '<b>Param botanist</b> - Récupère des conseils de la base de données postées par un botaniste.</br>' +
            '<b>Param plant</b> - Récupère des conseils de la base de données concernant à une plante.',
        tags: ['Conseils'],
        parameters: [
            {
                in: 'query',
                name: 'id',
                schema: {
                    type: 'integer',
                    description: 'L\'ID du conseil à récupérer.'
                },
                required: false
            },
            {
                in: 'query',
                name: 'botanist',
                schema: {
                    type: 'integer',
                    description: 'L\'ID du botaniste ayant écris le ou les conseils.'
                },
                required: false
            },
            {
                in: 'query',
                name: 'plant',
                schema: {
                    type: 'integer',
                    description: 'L\'ID de la plante concerné par les conseils.'
                },
                required: false
            }
        ],
        responses: {
            '200': {
                description: 'Succès - Le conseil a été récupéré avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                conseil: {
                                    type: 'object',
                                    description: 'Détails du conseil récupéré.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête incorrecte - La requête est invalide en raison de paramètres manquants.'
            },
            '404': {
                description: 'Conseil non trouvé - Le conseil spécifié est introuvable.'
            }
        }
    },
    put: {
        summary: 'Modifier un conseil',
        description: 'Permet de modifier un conseil existant dans la base de données.',
        tags: ['Conseils'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            conseil_id: {
                                type: 'integer',
                                description: "L'identifiant du conseil à modifier."
                            },
                            contenu: {
                                type: 'string',
                                description: 'Le nouveau contenu du conseil.'
                            }
                        },
                        required: ['conseil_id', 'contenu']
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - Le conseil a été modifié avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                },
                                conseil: {
                                    type: 'object',
                                    description: 'Détails du conseil modifié.'
                                }
                            }
                        }
                    }
                }
            },
            '404': {
                description: 'Conseil non trouvé - Le conseil spécifié n\'a pas été trouvé.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la modification du conseil.'
            }
        }
    },
    delete: {
        summary: 'Supprimer un conseil',
        description: 'Supprime un conseil de la base de données.',
        tags: ['Conseils'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            conseil_id: {
                                type: 'integer',
                                description: 'L\'ID du conseil à supprimer.'
                            }
                        }
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - Le conseil a été supprimé avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation de suppression.'
                                },
                                conseil: {
                                    type: 'object',
                                    description: 'Détails du conseil supprimé.'
                                }
                            }
                        }
                    }
                }
            },
            '404': {
                description: 'Conseil non trouvé - Le conseil spécifié est introuvable.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la suppression du conseil.'
            }
        }
    }
};

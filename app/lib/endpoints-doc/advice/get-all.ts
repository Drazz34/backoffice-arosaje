export const advice_get_all =
{
    get: {
        summary: 'Retourne tous les conseils',
        description: 'Retourne tous les conseils',
        tags: ['Conseils'],
        responses: {
        '200': {
            description: 'Conseils récupérés avec succès',
        },
        '401': {
            description: 'Non autorisé',
        },
        '500': {
            description: 'Erreur serveur',
        },
    }
    }
};
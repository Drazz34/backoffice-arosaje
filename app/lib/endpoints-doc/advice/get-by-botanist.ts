export const advice_get_by_botanist = {
    get: {
        summary: 'Obtenir les conseils d\'un botaniste',
        description: 'Récupère les conseils donnés par un botaniste spécifié.',
        tags: ['Conseils'],
        parameters: [
            {
                in: 'query',
                name: 'botanist',
                schema: {
                    type: 'integer',
                    description: 'L\'ID du botaniste dont on veut récupérer les conseils.'
                },
                required: true
            }
        ],
        responses: {
            '200': {
                description: 'Succès - Les conseils ont été récupérés avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                conseils: {
                                    type: 'array',
                                    items: {
                                        type: 'object',
                                        description: 'Détails du conseil donné par le botaniste.'
                                    }
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '401': {
                description: 'Non autorisé.'
            },
            '404': {
                description: 'Botaniste non trouvé - Le botaniste spécifié est introuvable ou n\'a donné aucun conseil.'
            }
        }
    }
};

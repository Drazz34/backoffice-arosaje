export const advice_get_by_plant = {
    get: {
        summary: 'Obtenir les conseils pour une plante',
        description: 'Récupère les conseils relatifs à une plante spécifiée.',
        tags: ['Conseils'],
        parameters: [
            {
                in: 'query',
                name: 'plant',
                schema: {
                    type: 'integer',
                    description: 'L\'ID de la plante pour laquelle récupérer les conseils.'
                },
                required: true
            }
        ],
        responses: {
            '200': {
                description: 'Succès - Les conseils relatifs à la plante ont été récupérés avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                conseils: {
                                    type: 'array',
                                    items: {
                                        type: 'object',
                                        description: 'Détails du conseil relatif à la plante.'
                                    }
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '401': {
                description: 'Non autorisé.'
            },
            '404': {
                description: 'Plante non trouvée - La plante spécifiée est introuvable ou n\'a aucun conseil associé.'
            }
        }
    }
};

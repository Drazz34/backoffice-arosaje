export const announcements = {
    post: {
        summary: 'Créer une annonce',
        description: 'Permet de créer une nouvelle annonce dans la base de données.',
        tags : ['Annonces'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            nom_plante: {
                                type: 'string',
                                description: 'Le nom de la plante associée à l\'annonce.'
                            },
                            description: {
                                type: 'string',
                                description: 'La description de la plante associée à l\'annonce.'
                            },
                            image: {
                                type: 'string',
                                description: 'Le chemin de l\'image associée à l\'annonce.'
                            },
                            date_debut: {
                                type: 'string',
                                format: 'date-time',
                                description: 'La date de début de l\'annonce.'
                            },
                            date_fin: {
                                type: 'string',
                                format: 'date-time',
                                description: 'La date de fin de l\'annonce.'
                            },
                            periodicite_entretien: {
                                type: 'string',
                                description: 'La périodicité d\'entretien de la plante associée à l\'annonce.'
                            },
                            proprietaire_id: {
                                type: 'integer',
                                description: 'L\'identifiant du propriétaire de l\'annonce.'
                            }
                        }
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - L\'annonce a été créée avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                },
                                annonce: {
                                    type: 'object',
                                    description: 'Détails de l\'annonce créée.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '401': {
                description: 'Non autorisé.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la création de l\'annonce.'
            }
        }
    },
    get: {
        summary: 'Obtenir une ou plusieurs annonce(s)',
        description:
            '<b>Sans param</b> - Récupère toutes les annonces de la base de données.</br>' +
            '<b>Param id</b> - Récupère une annonce de la base de données par son ID.</br>' +
            '<b>Param user</b> - Récupère des annonces de la base de données postées par un utiliseur.',
        tags: ['Annonces'],
        parameters: [
            {
                in: 'query',
                name: 'id',
                schema: {
                    type: 'integer',
                    description: "L'ID de l'annonce à récupérer."
                },
                required: false
            },
            {
                in: 'query',
                name: 'user',
                schema: {
                    type: 'integer',
                    description: "L'ID d'un utilisateur lié ayant posté une ou des annonces."
                },
                required: false
            }
        ],
        responses: {
            '200': {
                description: 'Succès - L\'annonce a été récupérée avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                annonce: {
                                    type: 'object',
                                    description: 'Détails de l\'annonce récupérée.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête incorrecte - La requête est invalide en raison de paramètres manquants.'
            },
            '404': {
                description: "Annonce non trouvée - L'annonce spécifiée n'a pas été trouvée."
            }
        }
    },
    put: {
        summary: 'Modifier une annonce',
        description: 'Permet de modifier une annonce existante dans la base de données.',
        tags: ['Annonces'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            annonce_id: {
                                type: 'integer',
                                description: 'L\'identifiant de l\'annonce à modifier.'
                            },
                            date_debut: {
                                type: 'string',
                                format: 'date-time',
                                description: 'La nouvelle date de début de l\'annonce.'
                            },
                            date_fin: {
                                type: 'string',
                                format: 'date-time',
                                description: 'La nouvelle date de fin de l\'annonce.'
                            },
                            periodicite_entretien: {
                                type: 'string',
                                description: 'La nouvelle périodicité d\'entretien de l\'annonce.'
                            },
                            gardien_id: {
                                type: 'integer',
                                description: 'Le nouvel identifiant du gardien de l\'annonce.'
                            }
                        }
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - L\'annonce a été modifiée avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                },
                                annonce: {
                                    type: 'object',
                                    description: 'Détails de l\'annonce modifiée.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Corps de la requête vide - Le corps de la requête est vide.'
            },
            '404': {
                description: 'Annonce introuvable - L\'annonce à modifier n\'a pas été trouvée.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la modification de l\'annonce.'
            }
        }
    },
    delete: {
        summary: 'Delete an announcements',
        description: 'Deletes an announcements from the database.',
        tags: ['Annonces'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            announcement_id: {
                                type: 'integer',
                                description: 'The ID of the announcements to delete.'
                            }
                        },
                        required: ['announcement_id']
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Success - The announcements has been deleted successfully.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Confirmation message.'
                                },
                                announcement: {
                                    type: 'object',
                                    description: 'Details of the deleted announcements.'
                                }
                            }
                        }
                    }
                }
            },
            '404': {
                description: 'Announcement not found - The specified announcements was not found.'
            },
            '500': {
                description: 'Internal Server Error - An error occurred while deleting the announcements.'
            }
        }
    }
};

export const announcement_get_all = {
    get: {
        summary: 'Liste des annonces',
        description: 'Permet d\'obtenir la liste de toutes les annonces disponibles, triées par date de début.',
        tags: ['Annonces'],
        responses: {
            '200': {
                description: 'Succès - Liste des annonces récupérée avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                annonces: {
                                    type: 'array',
                                    items: {
                                        type: 'object',
                                        description: 'Détails d\'une annonce.'
                                    }
                                }
                            }
                        }
                    }
                }
            },
            '401': {
                description: 'Non autorisé.'
            },
            '404': {
                description: 'Annonce introuvable - Aucune annonce n\'a été trouvée.'
            }
        }
    }
};

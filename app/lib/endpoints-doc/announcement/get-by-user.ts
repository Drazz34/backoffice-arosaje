export const announcement_get_by_user = {
    get: {
        summary: 'Obtenir les annonces par ID utilisateur',
        description: 'Récupère les annonces associées à un utilisateur dans la base de données par leur ID.',
        tags: ['Annonces'],
        parameters: [
            {
                in: 'query',
                name: 'user',
                schema: {
                    type: 'integer',
                    description: "L'ID de l'utilisateur pour lequel récupérer les annonces."
                },
                required: true
            }
        ],
        responses: {
            '200': {
                description: 'Succès - Les annonces associées à l\'utilisateur ont été récupérées avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                annonces: {
                                    type: 'array',
                                    items: {
                                        type: 'object',
                                        description: 'Détails d\'une annonce associée à l\'utilisateur.'
                                    }
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '401': {
                description: 'Non autorisé.'
            },
            '404': {
                description: 'Utilisateur non trouvé - L\'utilisateur spécifié est introuvable.'
            }
        }
    }
};

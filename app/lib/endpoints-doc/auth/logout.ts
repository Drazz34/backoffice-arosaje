export const logout = {
    get: {
        summary: 'Déconnexion',
        description: 'Permet à un utilisateur de se déconnecter de l\'application.',
        tags: ['Authentification'],
        responses: {
            '200': {
                description: 'Succès - L\'utilisateur a été déconnecté avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                }
                            }
                        }
                    }
                }
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la déconnexion de l\'utilisateur.'
            }
        }
    }
}
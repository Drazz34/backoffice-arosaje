export const signin = {
    post: {
        summary: 'Se connecter',
        description: 'Permet à un utilisateur de s\'authentifié et de récupérer un Json Web Token (JWT).',
        tags: ['Authentification'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            login: {
                                type: 'string',
                                format: 'email',
                                description: 'L\'adresse e-mail de l\'utilisateur.'
                            },
                            password: {
                                type: 'string',
                                format: 'password',
                                description: 'Le mot de passe de l\'utilisateur.'
                            }
                        },
                        required: ['login', 'password']
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - L\'utilisateur a été connecté avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                success: {
                                    type: 'boolean',
                                    description: 'Message de confirmation.'
                                },
                                token: {
                                    type: 'string',
                                    description: 'Token d\'accès.'
                                },
                                refreshToken: {
                                    type: 'string',
                                    description: 'Token de refresh.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '401': {
                description: 'Non autorisé - L\'adresse e-mail ou le mot de passe est incorrect.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la connexion de l\'utilisateur.'
            }
        }
    }
}
export const signup = {
    post: {
        summary: 'Créer un nouvel utilisateur',
        description: 'Permet à un utilisateur de créer un compte dans l\'application.',
        tags: ['Authentification'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            pseudo: {
                                type: 'string',
                                description: 'Le pseudo de l\'utilisateur.'
                            },
                            mail: {
                                type: 'string',
                                format: 'email',
                                description: 'L\'adresse e-mail de l\'utilisateur.'
                            },
                            mot_de_passe: {
                                type: 'string',
                                format: 'password',
                                description: 'Le mot de passe de l\'utilisateur.'
                            },
                            genre: {
                                type: 'integer',
                                description: 'L\'identifiant du genre de l\'utilisateur.'
                            },
                            image: {
                                type: 'string',
                                description: 'Le chemin de l\'image de profil de l\'utilisateur.'
                            },
                            rue: {
                                type: 'string',
                                description: 'Le nom de la rue de l\'adresse de l\'utilisateur.'
                            },
                            ville: {
                                type: 'string',
                                description: 'Le nom de la ville de l\'adresse de l\'utilisateur.'
                            },
                            code_postal: {
                                type: 'string',
                                description: 'Le code postal de l\'adresse de l\'utilisateur.'
                            },
                            est_botaniste: {
                                type: 'boolean',
                                description: 'Indique si l\'utilisateur est un botaniste ou non.'
                            },
                            est_admin: {
                                type: 'boolean',
                                description: 'Indique si l\'utilisateur est un administrateur ou non.'
                            }
                        }
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - L\'utilisateur a été créé avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la création de l\'utilisateur.'
            }
        }
    }
}
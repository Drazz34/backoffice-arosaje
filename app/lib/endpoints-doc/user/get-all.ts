export const user_get_all = {
    get: {
        summary: 'Liste des utilisateurs',
        description: 'Permet d\'obtenir la liste des utilisateurs enregistrés dans la base de données.',
        tags: ['Utilisateurs'],
        responses: {
            '200': {
                description: 'Succès - Liste des utilisateurs récupérée avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                users: {
                                    type: 'array',
                                    items: {
                                        type: 'object',
                                        description: 'Détails d\'un utilisateur.'
                                    }
                                }
                            }
                        }
                    }
                }
            },
            '401': {
                description: 'Non autorisé.'
            },
            '404': {
                description: 'Aucun utilisateur disponible - Aucun utilisateur n\'a été trouvé dans la base de données.'
            }
        }
    }
};

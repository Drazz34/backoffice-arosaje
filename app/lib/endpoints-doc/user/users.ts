export const users =
{
    post: {
        summary: 'Créer un nouvel utilisateur',
        description: 'Permet d\'ajouter un nouvel utilisateur à la base de données.',
        tags: ['Utilisateurs'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            pseudo: {
                                type: 'string',
                                description: 'Le pseudo de l\'utilisateur.'
                            },
                            mail: {
                                type: 'string',
                                format: 'email',
                                description: 'L\'adresse e-mail de l\'utilisateur.'
                            },
                            mot_de_passe: {
                                type: 'string',
                                format: 'password',
                                description: 'Le mot de passe de l\'utilisateur.'
                            },
                            genre: {
                                type: 'integer',
                                description: 'L\'identifiant du genre de l\'utilisateur.'
                            },
                            image: {
                                type: 'string',
                                description: 'Le chemin de l\'image de profil de l\'utilisateur.'
                            },
                            rue: {
                                type: 'string',
                                description: 'Le nom de la rue de l\'adresse de l\'utilisateur.'
                            },
                            ville: {
                                type: 'string',
                                description: 'Le nom de la ville de l\'adresse de l\'utilisateur.'
                            },
                            code_postal: {
                                type: 'string',
                                description: 'Le code postal de l\'adresse de l\'utilisateur.'
                            },
                            est_botaniste: {
                                type: 'boolean',
                                description: 'Indique si l\'utilisateur est un botaniste ou non.'
                            },
                            est_admin: {
                                type: 'boolean',
                                description: 'Indique si l\'utilisateur est un administrateur ou non.'
                            }
                        }
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - L\'utilisateur a été ajouté avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                },
                                user: {
                                    type: 'object',
                                    description: 'Détails de l\'utilisateur ajouté.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête invalide. Le corps de la requête est incorrect ou incomplet.'
            },
            '401': {
                description: 'Non autorisé.'
            },
            '404': {
                description: 'Le genre n\'a pas été trouvé.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de l\'ajout de l\'utilisateur.'
            }
        }
    },
    get: {
        summary: 'Obtenir un ou plusieurs utilisateur(s)',
        description:
            '<b>Sans param</b> - Récupère tous les utilisateurs de la base de données.</br>' +
            '<b>Param id</b> - Récupère un utilisateur de la base de données par son ID.',
        tags: ['Utilisateurs'],
        parameters: [
            {
                in: 'query',
                name: 'id',
                schema: {
                    type: 'integer',
                    description: "L'ID de l'utilisateur à récupérer."
                },
                required: false
            }
        ],
        responses: {
            '200': {
                description: 'Succès - L\'utilisateur a été récupéré avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                user: {
                                    type: 'object',
                                    description: 'Détails de l\'utilisateur récupéré.'
                                }
                            }
                        }
                    }
                }
            },
            '400': {
                description: 'Requête incorrecte - La requête est invalide en raison de paramètres manquants.'
            },
            '404': {
                description: 'Utilisateur non trouvé - L\'utilisateur spécifié n\'a pas été trouvé.'
            }
        }
    },
    put: {
        summary: 'Modifier un utilisateur existant',
        description: 'Permet de modifier les informations d\'un utilisateur existant dans la base de données.',
        tags: ['Utilisateurs'],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            user: {
                                type: 'number',
                                description: 'L\'identifiant de l\'utilisateur à modifier.'
                            },
                            pseudo: {
                                type: 'string',
                                description: 'Le nouveau pseudo de l\'utilisateur.'
                            },
                            mail: {
                                type: 'string',
                                format: 'email',
                                description: 'La nouvelle adresse e-mail de l\'utilisateur.'
                            },
                            est_botaniste: {
                                type: 'boolean',
                                description: 'Le nouveau statut de botaniste de l\'utilisateur.'
                            },
                            est_admin: {
                                type: 'boolean',
                                description: 'Le nouveau statut d\'administrateur de l\'utilisateur.'
                            }
                        }
                    }
                }
            }
        },
        responses: {
            '200': {
                description: 'Succès - L\'utilisateur a été modifié avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                },
                                user: {
                                    type: 'object',
                                    description: 'Détails de l\'utilisateur modifié.'
                                }
                            }
                        }
                    }
                }
            },
            '404': {
                description: 'L\'utilisateur n\'a pas été trouvé.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la modification de l\'utilisateur.'
            }
        }
    },
    delete: {
        summary: 'Supprimer un utilisateur',
        description: 'Permet de supprimer un utilisateur enregistré dans la base de données.',
        tags: ['Utilisateurs'],
        parameters: [
            {
                in: 'query',
                name: 'id',
                schema: {
                    type: 'integer',
                    description: "L'ID de l'utilisateur à supprimer."
                },
                required: true
            }
        ],
        responses: {
            '200': {
                description: 'Succès - L\'utilisateur a été supprimé avec succès.',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                message: {
                                    type: 'string',
                                    description: 'Message de confirmation.'
                                },
                                user: {
                                    type: 'object',
                                    description: 'Détails de l\'utilisateur supprimé.'
                                }
                            }
                        }
                    }
                }
            },
            '404': {
                description: 'Utilisateur introuvable - Aucun utilisateur correspondant à l\'identifiant spécifié n\'a été trouvé.'
            },
            '500': {
                description: 'Erreur interne du serveur - Une erreur s\'est produite lors de la suppression de l\'utilisateur.'
            }
        }
    }
};
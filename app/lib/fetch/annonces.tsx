import {Annonce, Plante, Image, Utilisateur, Adresse, Entretien, Ville, CodePostal, Genre} from "@/db/models";
import { AnnonceVM } from "../view_models/AnnonceVM";
import { ImageVM } from "../view_models/ImageVM";
import { UtilisateurVM } from "../view_models/UtilisateurVM";
import { PlanteVM } from "../view_models/PlanteVM";

async function fetchAllAnnonces() {

  const annoncesVM: Array<AnnonceVM> = [];
  const annonces = await Annonce.findAll({
    include: [
      {
        model: Image,
      },
      {
        model: Plante,
      },
      {
        model: Utilisateur,
        as: 'Proprietaire',
        include: [
          {
            model: Adresse,
            include: [
                {
                    model: Ville,
                },
                {
                    model: CodePostal,
                }
            ]
          },
          {
            model: Genre,
          },
          {
            model: Image,
          }
        ]
      },
      {
        model: Utilisateur,
        as: 'Gardien',
        include: [
          {
            model: Adresse,
            include: [
              {
                model: Ville,
              },
              {
                model: CodePostal,
              }
            ]
          },
          {
            model: Genre,
          },
          {
            model: Image,
          }
        ]
      },
      {
        model: Entretien,
        include: [
          {
            model: Image,
          }
        ]
      }
    ]
  });

  annonces.map(async (annonce) => {
    annoncesVM.push({
      id: annonce.id,
      dateDebut: annonce.date_debut,
      dateFin: annonce.date_fin,
      periodiciteEntretien: annonce.periodicite_entretien,
      dateCreation: annonce.date_creation,
      dateModif: annonce.date_modif,
      image: {
        id: annonce.Image.id,
        chemin: annonce.Image.chemin
      },
      plante: {
        id: annonce.Plante.id,
        nomPlante: annonce.Plante.nom_plante,
        description: annonce.Plante.description
      },
      proprietaire: {
        id: annonce.Proprietaire.id,
        pseudo: annonce.Proprietaire.pseudo,
        mail: annonce.Proprietaire.mail,
        estAdmin: annonce.Proprietaire.est_admin,
        estBotaniste: annonce.Proprietaire.est_botaniste,
        dateCreation: annonce.Proprietaire.date_creation,
        dateModif: annonce.Proprietaire.date_modif,
        adresse: {
          id: annonce.Proprietaire.Adresse.id,
          rue: annonce.Proprietaire.Adresse.rue,
        },
        ville: {
          id: annonce.Proprietaire.Adresse.Ville.id,
          nomVille: annonce.Proprietaire.Adresse.Ville.nom_ville
        },
        codePostal: {
          id: annonce.Proprietaire.Adresse.CodePostal.id,
          codePostal: annonce.Proprietaire.Adresse.CodePostal.code_postal
        },
        genre: {
          id: annonce.Proprietaire.Genre.id,
          sexe: annonce.Proprietaire.Genre.sexe
        },
        image: {
          id: annonce.Proprietaire.Image.id,
          chemin: annonce.Proprietaire.Image.chemin
        },
      },
      gardien: {
        id: annonce.Gardien.id,
        pseudo: annonce.Gardien.pseudo,
        mail: annonce.Gardien.mail,
        estBotaniste: annonce.Proprietaire.est_botaniste,
        estAdmin: annonce.Gardien.est_admin,
        dateCreation: annonce.Gardien.date_creation,
        dateModif: annonce.Gardien.date_modif,
        adresse: {
          id: annonce.Gardien.Adresse.id,
          rue: annonce.Gardien.Adresse.rue,
        },
        ville: {
          id: annonce.Gardien.Adresse.Ville.id,
          nomVille: annonce.Gardien.Adresse.Ville.nom_ville
        },
        codePostal: {
          id: annonce.Gardien.Adresse.CodePostal.id,
          codePostal: annonce.Gardien.Adresse.CodePostal.code_postal
        },
        genre: {
          id: annonce.Gardien.Genre.id,
          sexe: annonce.Gardien.Genre.sexe
        },
        image: {
          id: annonce.Gardien.Image.id,
          chemin: annonce.Gardien.Image.chemin
        },
      },
    });
  });
  return annoncesVM;
}

async function fetchDashboardAnnonces() {

  const annoncesVM: Array<AnnonceVM> = [];
  const annonces = await Annonce.findAll({
    limit: 10,
    include: [
      {
        model: Image,
      },
      {
        model: Plante,
      },
      {
        model: Utilisateur,
        as: 'Proprietaire',
        include: [
          {
            model: Image,
          }
        ]
      },
      {
        model: Utilisateur,
        as: 'Gardien',
        include: [
          {
            model: Image,
          }
        ]
      },
    ]
  });

  annonces.map(async (annonce) => {
    annoncesVM.push({
      id: annonce.id,
      dateDebut: annonce.date_debut,
      dateFin: annonce.date_fin,
      periodiciteEntretien: annonce.periodicite_entretien,
      dateCreation: annonce.date_creation,
      dateModif: annonce.date_modif,
      image: {
        id: annonce.Image.id,
        chemin: annonce.Image.chemin
      },
      plante: {
        id: annonce.Plante.id,
        nomPlante: annonce.Plante.nom_plante,
        description: annonce.Plante.description
      },
      proprietaire: {
        id: annonce.Proprietaire.id,
        pseudo: annonce.Proprietaire.pseudo,
        mail: annonce.Proprietaire.mail,
        estBotaniste: annonce.Proprietaire.est_botaniste,
        estAdmin: annonce.Proprietaire.est_admin,
        dateCreation: annonce.Proprietaire.date_creation,
        dateModif: annonce.Proprietaire.date_modif,
        image: {
          id: annonce.Proprietaire.Image.id,
          chemin: annonce.Proprietaire.Image.chemin
        },
      },
      gardien: {
        id: annonce.Gardien.id,
        pseudo: annonce.Gardien.pseudo,
        mail: annonce.Gardien.mail,
        estBotaniste: annonce.Proprietaire.est_botaniste,
        estAdmin: annonce.Gardien.est_admin,
        dateCreation: annonce.Gardien.date_creation,
        dateModif: annonce.Gardien.date_modif,
        image: {
          id: annonce.Gardien.Image.id,
          chemin: annonce.Gardien.Image.chemin
        },
      },
    });
  });
  return annoncesVM;
}

export { fetchAllAnnonces, fetchDashboardAnnonces };
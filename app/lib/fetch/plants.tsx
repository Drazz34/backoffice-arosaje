import Plante from "@/db/models/Plante"
import { PlanteVM } from "../view_models/PlanteVM";

export async function fetchAllPlants() {
  const plants = await Plante.findAll();
  const plantsVM: Array<PlanteVM> = [];
  plants.map(plant => {
    plantsVM.push({
      id: plant.getDataValue("id"),
      nomPlante: plant.getDataValue("nom_plante"),
      description: plant.getDataValue("description")
    });
  });
  return plantsVM;
}
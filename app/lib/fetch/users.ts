import {Utilisateur, Image, Adresse, Ville, CodePostal} from "@/db/models";
import {UtilisateurVM} from "@/app/lib/view_models/UtilisateurVM";
import Sequelize from 'sequelize';

const ITEMS_PER_PAGE = 10;

async function fetchFilteredUsers(
    query: string,
    currentPage: number,
){
    // await new Promise((resolve) => setTimeout(resolve, 3000));
    const offset = (currentPage - 1) * ITEMS_PER_PAGE;
    const users = await Utilisateur.findAll({
        limit: ITEMS_PER_PAGE,
        offset: offset,
        where: {
            [Sequelize.Op.or]: [
                {
                    pseudo: {
                        [Sequelize.Op.like]: `%${query}%`
                    }
                },
                {
                    mail: {
                        [Sequelize.Op.like]: `%${query}%`
                    }
                }
            ]
        },
        include: [
            {
                model: Image,
            },
            {
                model: Adresse,
                include: [
                    {
                        model: Ville,
                        // where: {
                        //     [Sequelize.Op.or]: [
                        //         {
                        //             nom_ville: {
                        //                 [Sequelize.Op.like]: `%${query}%`
                        //             }
                        //         }
                        //     ]
                        // },
                        // required: false,
                    },
                    {
                        model: CodePostal,
                    }
                ]
            },
        ]
    });

    const usersVM: Array<UtilisateurVM> = [];

    users.map(async (user) => {
        usersVM.push({
            id: user.id,
            pseudo: user.pseudo,
            mail: user.mail,
            estBotaniste: user.est_botaniste,
            estAdmin: user.est_admin,
            dateCreation: user.date_creation,
            dateModif: user.date_modif,
            image: {
                id: user.Image.id,
                chemin: user.Image.chemin
            },
            ville: {
                id: user.Adresse.Ville?.id,
                nomVille: user.Adresse.Ville?.nom_ville
            },
        });
    });

    return usersVM;
}

async function fetchUsersPage(query: string) {
    const count = (await Utilisateur.findAndCountAll({
        where: {
            pseudo: {
                [Sequelize.Op.like]: `%${query}%`
            }
        }
    })).count;

    return Math.ceil(Number(count) / ITEMS_PER_PAGE);
}

export {fetchFilteredUsers, fetchUsersPage};
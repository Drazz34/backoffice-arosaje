import {createSwaggerSpec} from 'next-swagger-doc';
import {users} from "@/app/lib/endpoints-doc/user/users";
import {announcements} from "@/app/lib/endpoints-doc/announcement/announcements";
import {advices} from "@/app/lib/endpoints-doc/advice/advices";
import {signin} from "@/app/lib/endpoints-doc/auth/signin";
import {signup} from "@/app/lib/endpoints-doc/auth/signup";
import {logout} from "@/app/lib/endpoints-doc/auth/logout";

export const getApiDocs = async () => {
    return createSwaggerSpec({
        apiFolder: 'app/api', // define api folder under app folder
        definition: {
            openapi: '3.0.0',
            info: {
                title: 'Arosaje API',
                version: '1.0',
            },
            components: {
                securitySchemes: {
                    BearerAuth: {
                        type: 'http',
                        scheme: 'bearer',
                        bearerFormat: 'JWT',
                    },
                },
            },
            tags: [
                {
                    name: 'Authentification',
                    description: 'Opérations relatives à l\'authentification'
                },
                {
                    name: 'Utilisateurs',
                    description: 'Opérations relatives aux utilisateurs'
                },
                {
                    name: 'Annonces',
                    description: 'Opérations relatives aux annonces'
                },
                {
                    name: 'Conseils',
                    description: 'Opérations relatives aux conseils'
                }
            ],
            paths: {
                // Authentification
                '/api/auth/signin': signin,
                '/api/auth/signup': signup,
                '/api/auth/logout': logout,
                // Utilisateurs
                '/api/users': users,
                // Annonces
                '/api/announcements': announcements,
                // Conseils
                '/api/advices': advices,
            },
            security: [
                {
                    BearerAuth: [],
                },
            ],
        },
    });
};
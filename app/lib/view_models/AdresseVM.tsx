import { CodePostalVM } from "./CodePostalVM"
import { VilleVM } from "./VilleVM"

export type AdresseVM = {
  id: number,
  rue: string,
}
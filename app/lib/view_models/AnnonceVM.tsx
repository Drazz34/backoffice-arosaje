import { ImageVM } from "./ImageVM"
import { PlanteVM } from "./PlanteVM"
import { UtilisateurVM } from "./UtilisateurVM"

export type AnnonceVM = {
  id: number,
  dateDebut: Date,
  dateFin: Date,
  periodiciteEntretien: number,
  dateCreation: Date,
  dateModif?: Date,
  image: ImageVM,
  plante: PlanteVM,
  proprietaire?: UtilisateurVM,
  gardien?: UtilisateurVM
}
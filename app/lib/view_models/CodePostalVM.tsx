export type CodePostalVM = {
  id: number,
  codePostal: string
}
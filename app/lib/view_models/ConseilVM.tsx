import { PlanteVM } from "./PlanteVM"
import { UtilisateurVM } from "./UtilisateurVM"

export type Conseil = {
  id: number,
  contenu: string,
  dateCreation: Date,
  dateModif?: Date,
  plante: PlanteVM,
  botaniste: UtilisateurVM
}
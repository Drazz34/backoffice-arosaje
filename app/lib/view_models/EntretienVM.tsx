import { AnnonceVM } from "./AnnonceVM"
import { ImageVM } from "./ImageVM"

export type EntretienVM = {
  id: number,
  dateEntretien: Date,
  annonce: AnnonceVM,
  image: ImageVM
}
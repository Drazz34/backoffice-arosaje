export type PlanteVM = {
  id: number,
  nomPlante: string,
  description?: string
}
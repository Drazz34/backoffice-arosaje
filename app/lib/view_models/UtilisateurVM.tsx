import { AdresseVM } from "./AdresseVM"
import { GenreVM } from "./GenreVM"
import { ImageVM } from "./ImageVM"
import { VilleVM } from "./VilleVM"
import { CodePostalVM } from "./CodePostalVM"

export type UtilisateurVM = {
  id: number,
  pseudo: string,
  mail: string,
  estBotaniste: boolean,
  estAdmin: boolean,
  dateCreation: Date,
  dateModif?: Date,
  adresse?: AdresseVM,
  ville?: VilleVM,
  codePostal?: CodePostalVM,
  genre?: GenreVM,
  image: ImageVM,
}
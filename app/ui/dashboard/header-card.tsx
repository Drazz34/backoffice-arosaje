import { quicksand } from "../fonts";

export default function HeaderCard({ content, count }: { content: string, count: number }) {
 return (
  <div className={`${quicksand.className} antialiased rounded-md border-[10px] border-[var(--light-green)] xl:p-4 p-2 xl:text-2xl md:text-xl`}>
   <span className={`pe-2`}>{ count }</span><span>{ content }</span>
  </div>
 );
}
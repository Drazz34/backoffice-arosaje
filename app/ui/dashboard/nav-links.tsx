'use client';

import clsx from "clsx";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";

const links = [
  { name: 'Accueil', href: "/dashboard", iconPath: "/icon/home.svg", iconAlt: "Home icon"},
  { name: 'Plantes', href: "/dashboard/plants", iconPath: "/icon/plant.svg", iconAlt: "Plant icon"},
  { name: 'Conseils', href: "/dashboard/advices", iconPath: "/icon/advice.svg", iconAlt: "Advice icon"},
  { name: 'Utilisateurs', href: "/dashboard/users", iconPath: "/icon/user.svg", iconAlt: "User icon"},
];

export default function NavLinks() {
  const pathname = usePathname();
  return (
    <nav className='min-w-max'>
      <ul className='list-none'>
        <>
          {
            links.map(link => {
              return(
                <li key={link.name}
                  className='my-6'>
                  <Link
                    href={link.href}
                    className={clsx(
                      'flex items-center gap-4 rounded-3xl px-6 py-1 bg-gradient-to-r bg-[var(--light-green)] hover:bg-gradient-to-r hover:from-green-200 hover:to-emerald-50',
                      {
                        'bg-gradient-to-r from-green-200 to-emerald-50': pathname === link.href
                      },
                    )}
                  >
                    <Image
                      src={link.iconPath}
                      width={30}
                      height={30}
                      alt={link.iconAlt}
                    />
                    <span className='font-light'>{link.name}</span>
                  </Link>
                </li>
              )
            })
          }
        </>
      </ul>
    </nav>
  );
}
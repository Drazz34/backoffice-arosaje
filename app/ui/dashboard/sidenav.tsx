import Image from 'next/image';
import Link from 'next/link';
import NavLinks from './nav-links';

export default function SideNav() {
  return (
    <aside className='min-w-max flex flex-col items-center justify-end gap-10 p-4'>
      <div>
        <Image
          src="/icon/logo.svg"
          width={150}
          height={150}
          alt='App logo'
        />
      </div>
      <NavLinks />
      <Link
          className='flex items-center gap-4 mt-auto mb-20 rounded-3xl px-6 py-1 bg-[var(--light-green)] hover:bg-gradient-to-r from-green-200 to-emerald-50'
          href={"/"}
        >
          <Image
            src="/icon/sign-out.svg"
            width={30}
            height={30}
            alt='Sign out icon'
          />
          <span className='font-light'>Déconnexion</span>
        </Link>
    </aside>
  );
}
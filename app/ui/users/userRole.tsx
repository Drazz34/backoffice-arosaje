import {UtilisateurVM} from "@/app/lib/view_models/UtilisateurVM";

export default function UserRole({ user }: { user: UtilisateurVM }) {
    if (user.estAdmin && user.estBotaniste) {
        return (
            <div className={"flex justify-center gap-1"}>
                <span className={"rounded-full bg-[var(--black)] text-white px-2"}>
                    Admin
                </span>
                <span className={"rounded-full bg-[var(--green)] text-white px-2"}>
                    Botaniste
                </span>
            </div>
        )
    } else if (user.estAdmin) {
        return (
            <div className={"flex justify-center gap-1"}>
                <span className={"rounded-full bg-[var(--black)] text-white px-2"}>
                    Admin
                </span>
            </div>
        )
    } else if (user.estBotaniste) {
        return (
            <div className={"flex justify-center gap-1"}>
                <span className={"rounded-full bg-[var(--green)] text-white px-2"}>
                    Botaniste
                </span>
            </div>
        )
    } else {
        return (
            <div className={"flex justify-center gap-1"}>
                <span className={"rounded-full bg-[var(--pink)] text-white px-2"}>
                    Utilisateur
                </span>
            </div>
        )
    }
}

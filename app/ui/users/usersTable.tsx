import Image from 'next/image';
import Link from "next/link";
import { formatDateToLocal } from '@/app/lib/utils';
import { fetchFilteredUsers } from "@/app/lib/fetch/users";
import UserRole from "@/app/ui/users/userRole";

export default async function UsersTable({
 query,
 currentPage,
}: {
    query: string;
    currentPage: number;
}) {
    const users = await fetchFilteredUsers(query, currentPage);

    return (
        <div className="mt-6 flow-root">
            <div className="inline-block min-w-full align-middle">
                <div className="rounded-lg bg-[var(--light-green)] p-2 md:pt-0">
                    <div className="md:hidden">
                        {users?.map((user) => (
                            <div
                                key={user.id}
                                className="mb-2 w-full rounded-md bg-white p-4"
                            >
                                <div className="flex items-center justify-between border-b pb-4">
                                    <div>
                                        <div className="mb-2 flex items-center">
                                            <Image
                                                src={`${user.image.chemin}`}
                                                className="mr-2 rounded-full"
                                                width={28}
                                                height={28}
                                                alt={`${user.pseudo}'s profile picture`}
                                            />
                                            <p>{user.pseudo}</p>
                                        </div>
                                        <p className="text-sm text-gray-500">{user.mail}</p>
                                    </div>
                                    {/*<InvoiceStatus status={users.status} />*/}
                                </div>
                                <div className="flex w-full items-center justify-between pt-4">
                                    <div>
                                        <p>{formatDateToLocal(user.dateCreation)}</p>
                                    </div>
                                    <div className="flex justify-end gap-2">
                                        {/*<UpdateInvoice id={users.id} />*/}
                                        {/*<DeleteInvoice id={users.id} />*/}
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                    <table className="hidden min-w-full text-gray-900 md:table">
                        <thead className="rounded-lg text-left text-sm font-normal">
                        <tr>
                            <th scope="col" className="px-4 py-5 font-medium sm:pl-6">
                                Pseudo
                            </th>
                            <th scope="col" className="px-3 py-5 font-medium">
                                Email
                            </th>
                            <th scope="col" className="px-3 py-5 font-medium">
                                Ville
                            </th>
                            <th scope="col" className="px-3 py-5 font-medium text-center">
                                Role
                            </th>
                            <th scope="col" className="px-3 py-5 font-medium">
                                {/* eslint-disable-next-line react/no-unescaped-entities */}
                                Date d'inscription
                            </th>
                        </tr>
                        </thead>
                        <tbody className="bg-[var(--white)]">
                        {users?.map((user) => (
                            <tr
                                key={user.id}
                                className="w-full border-b-8 border-b-[var(--light-green)] text-sm last-of-type:border-none [&:first-child>td:first-child]:rounded-tl-lg [&:first-child>td:last-child]:rounded-tr-lg [&:last-child>td:first-child]:rounded-bl-lg [&:last-child>td:last-child]:rounded-br-lg"
                            >
                                <td className="whitespace-nowrap py-3 pl-6 pr-3">
                                    <div className="flex items-center gap-3">
                                        <Image
                                            src={`${user.image.chemin}`}
                                            className="rounded-full"
                                            width={28}
                                            height={28}
                                            // alt={`${users.pseudo}'s profile picture`}
                                            alt={""}
                                        />
                                        <p>{user.pseudo}</p>
                                    </div>
                                </td>
                                <td className="whitespace-nowrap px-3 py-3">
                                    {user.mail}
                                </td>
                                <td className="whitespace-nowrap px-3 py-3">
                                    {user.ville?.nomVille}
                                </td>
                                <td className="whitespace-nowrap px-3 py-3">
                                    {/*<InvoiceStatus status={users.status} />*/}
                                    <UserRole user={user} />
                                </td>
                                <td className="whitespace-nowrap px-3 py-3">
                                    {formatDateToLocal(user.dateCreation)}
                                </td>
                                <td className="whitespace-nowrap px-3 py-3">
                                    <div className="flex gap-3">
                                        <Link
                                            href={`/dashboard/users/${user.id}`}
                                        >
                                            <Image
                                                src="/icon/edit.svg"
                                                alt="Edit"
                                                width={25}
                                                height={25}
                                            />
                                        </Link>
                                        <button>
                                            <Image
                                                src="/icon/delete.svg"
                                                alt="Delete"
                                                width={25}
                                                height={25}
                                            />
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
const authConfig = {
	saltRounds: 12,
	jwtExpires: 15 * 60, // 10 mins
	refreshTokenExpires: 7 * 24 * 60 * 60, // 1 day
};

export default authConfig;
'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('adresse', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rue: {
        type: Sequelize.STRING
      },
      ville_id: {
        foreignKey: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'ville',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      code_postal_id: {
        foreignKey: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'code_postal',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('adresse');
  }
};
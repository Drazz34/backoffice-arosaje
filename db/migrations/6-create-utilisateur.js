'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('utilisateur', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      pseudo: {
        type: Sequelize.STRING
      },
      mail: {
        type: Sequelize.STRING,
        unique: true,
      },
      mot_de_passe: {
        type: Sequelize.STRING
      },
      est_admin: {
        type: Sequelize.BOOLEAN,
      },
      est_botaniste: {
        type: Sequelize.BOOLEAN
      },
      date_creation: {
        type: Sequelize.DATE
      },
      date_modif: {
        type: Sequelize.DATE
      },
      genre_id: {
        foreignKey: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'genre',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      adresse_id: {
        foreignKey: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'adresse',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      image_id: {
        foreignKey: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'image',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
      // foreignKey image_id à ajouter
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('utilisateur');
  }
};
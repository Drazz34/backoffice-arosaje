'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('conseil', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      contenu: {
        type: Sequelize.STRING
      },
      date_creation: {
        type: Sequelize.DATE
      },
      date_modif: {
        type: Sequelize.DATE,
        allowNull: true
      },
      plante_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'plante',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      botaniste_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'utilisateur',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('conseil');
  }
};
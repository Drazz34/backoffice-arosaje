'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('annonce', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date_debut: {
        type: Sequelize.DATE
      },
      date_fin: {
        type: Sequelize.DATE
      },
      periodicite_entretien: {
        type: Sequelize.INTEGER
      },
      date_creation: {
        type: Sequelize.DATE
      },
      date_modif: {
        type: Sequelize.DATE,
        allowNull: true
      },
      image_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'image',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      plante_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'plante',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      proprietaire_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'utilisateur',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      gardien_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'utilisateur',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('annonce');
  }
};
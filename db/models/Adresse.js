import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const Adresse = (sequelize, DataTypes) => {
  class Adresse extends Model {
    id;
    rue;
    ville_id;
    code_postal_id;
    Ville;
    CodePostal;
  }
  Adresse.init({
    rue: DataTypes.STRING,
    ville_id: {
      type: DataTypes.STRING,
      references: {
        model: 'ville',
        key: 'id'
      }
    },
    code_postal_id: {
      type: DataTypes.STRING,
      references: {
        model: 'ville',
        key: 'id'
      }
    }
  }, {
    sequelize,
    modelName: 'Adresse',
    tableName: 'adresse',
    timestamps: false,
  });
  return Adresse;
};

export default Adresse(connection, DataTypes);
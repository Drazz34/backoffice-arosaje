import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const Annonce = (sequelize, DataTypes) => {
  class Annonce extends Model {
    id;
    date_debut;
    date_fin;
    periodicite_entretien;
    date_creation;
    date_modif;
    image_id;
    plante_id;
    proprietaire_id;
    gardien_id;
    Image;
    Plante;
    Proprietaire;
    Gardien;
  }
  Annonce.init({
    date_debut: DataTypes.DATE,
    date_fin: DataTypes.DATE,
    periodicite_entretien: DataTypes.INTEGER,
    date_creation: DataTypes.DATE,
    date_modif: {
      type: DataTypes.DATE,
      allowNull: true
    },
    image_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'image',
        key: 'id'
      }
    },
    plante_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'plante',
        key: 'id'
      }
    },
    proprietaire_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'utilisateur',
        key: 'id'
      }
    },
    gardien_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'utilisateur',
        key: 'id'
      }
    }
  }, {
    sequelize,
    modelName: 'Annonce',
    tableName: 'annonce',
    timestamps: false
  });
  return Annonce;
};

export default Annonce(connection, DataTypes);
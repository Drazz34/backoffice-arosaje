import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const CodePostal = (sequelize, DataTypes) => {
  class CodePostal extends Model {
    id;
    code_postal;
  }
  CodePostal.init({
    code_postal: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CodePostal',
    tableName: 'code_postal',
    timestamps: false,
  });
  return CodePostal;
};

export default CodePostal(connection, DataTypes);
import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const Conseil = (sequelize, DataTypes) => {
  class Conseil extends Model {
    id;
    contenu;
    date_creation;
    date_modif;
    plante_id;
    botaniste_id;
    Plante;
    Botaniste;
  }
  Conseil.init({
    contenu: DataTypes.STRING,
    date_creation: DataTypes.DATE,
    date_modif: {
      type: DataTypes.DATE,
      allowNull: true
    },
    plante_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'plante',
        key: 'id'
      }
    },
    botaniste_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'utilisateur',
        key: 'id'
      },
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Conseil',
    tableName: 'conseil',
    timestamps: false
  });
  return Conseil;
};

export default Conseil(connection, DataTypes);
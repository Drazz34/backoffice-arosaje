import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const Entretien = (sequelize, DataTypes) => {
  class Entretien extends Model {
    id;
    date_entretien;
    annonce_id;
    image_id;
    Annonce;
    Image;
  }
  Entretien.init({
    annonce_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'annonce',
        key: 'id'
      }
    },
    image_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'image',
        key: 'id'
      }
    },
    date_entretien: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Entretien',
    tableName: 'entretien',
    timestamps: false
  });
  return Entretien;
};

export default Entretien(connection, DataTypes);
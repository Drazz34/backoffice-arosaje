import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const Genre = (sequelize, DataTypes) => {
  class Genre extends Model {
    id;
    sexe;
  }
  Genre.init({
    sexe: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Genre',
    tableName: 'genre',
    timestamps: false,
  });
  return Genre;
};

export default Genre(connection, DataTypes);
import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const Image = (sequelize, DataTypes) => {
  class Image extends Model {
    id;
    chemin;
  }
  Image.init({
    chemin: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Image',
    tableName: 'image',
    timestamps: false
  });
  return Image;
};

export default Image(connection, DataTypes);
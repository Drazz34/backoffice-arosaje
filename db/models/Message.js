import { Model, DataTypes } from 'sequelize';
import connection from '../connection';

const Message = (sequelize, DataTypes) => {
  class Message extends Model {
    id;
    contenu;
    date_creation;
    expediteur_id;
    destinataire_id;
  }
  Message.init({
    contenu: DataTypes.TEXT,
    date_creation: DataTypes.DATE,
    expediteur_id: DataTypes.INTEGER,
    destinataire_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Message',
    tableName: 'message',
    timestamps: false
  });
  return Message;
};

export default Message(connection, DataTypes);
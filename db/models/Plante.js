import { Model, DataTypes } from 'sequelize';
import connection from '../connection';

const Plante = (sequelize, DataTypes) => {
  class Plante extends Model {
    id;
    nom_plante;
    description;
  }
  Plante.init({
    nom_plante: DataTypes.STRING,
    description: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Plante',
    tableName: 'plante',
    timestamps: false
  });
  return Plante;
};

export default Plante(connection, DataTypes);
'use strict';
import connection from "@/db/connection";
import {DataTypes, Model} from "sequelize";

const RefreshToken = (sequelize, DataTypes) => {
  class RefreshToken extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  RefreshToken.init({
    token: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    utilisateur_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "utilisateur",
        key: "id"
      }
    }
  }, {
    sequelize,
    modelName: 'RefreshToken',
    tableName: 'refresh_token',
    timestamps: false,
  });
  return RefreshToken;
};

export default RefreshToken(connection, DataTypes);
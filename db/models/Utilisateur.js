import {DataTypes, Model} from 'sequelize';
import connection from '../connection';
import {compareSync} from 'bcrypt';
import {getJwt, logout} from '@/app/lib/auth/auth';

const Utilisateur = (sequelize, DataTypes) => {
  class Utilisateur extends Model {
    id;
    pseudo;
    mail;
    mot_de_passe;
    est_botaniste;
    est_admin;
    date_creation;
    date_modif;
    adresse_id;
    genre_id;
    image_id;
    Adresse;
    Genre;
    Image;

    static async getByLoginId(loginId) {
      return await Utilisateur.findOne({
        attributes: {
          include: ['pseudo', 'mail', 'mot_de_passe']
        },
        where: {
          mail: loginId.toLowerCase(),
        },
      });
    }

    verifyPassword(password) {
      return compareSync(password, this.mot_de_passe);
    }

    static async login(login, password) {
      try {
        const user = await Utilisateur.getByLoginId(login);

        if (!user) {
          throw new Error();
        }

        // users.lastLogin = new Date();
        // users.lastSeen = new Date();
        // await users.save();

        const isPasswordValid = user.verifyPassword(password);

        if (!isPasswordValid) {
          throw new Error();
        }

        return user;
      } catch (error) {
        console.log(error);
        throw new Error('Identifiant ou mot de passe invalide.');
      }
    }

    static async getAuthUserFromDb() {
      const jwt = await getJwt();

      if (!jwt) {
        return null;
      }

      const user = await Utilisateur.findByPk(jwt.id);

      if (!user) {
        logout();
        return null;
      }

      return user;
    }

    static filterPublic(user) {
      const { password, ...userPublic } = user;
      return userPublic;
    }

    exportPublic() {
      const {
        mot_de_passe,
        adresse_id,
        genre_id,
        image_id,
        ...user
      } = this.toJSON();
      return user;
    }
  }
  Utilisateur.init({
    pseudo: DataTypes.STRING,
    mail: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    mot_de_passe: {
      type: DataTypes.STRING,
    },
    est_botaniste: DataTypes.BOOLEAN,
    est_admin: DataTypes.BOOLEAN,
    date_creation: DataTypes.DATE,
    date_modif: DataTypes.DATE,
    adresse_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "adresse",
        key: "id"
      }
    },
    genre_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "genre",
        key: "id"
      }
    },
    image_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "image",
        key: "id"
      }
    }
  }, {
    defaultScope: {
      attributes: {
        exclude: ["mot_de_passe"]
      }
    },
    sequelize,
    modelName: 'Utilisateur',
    tableName: 'utilisateur',
    timestamps: false,
  });
  return Utilisateur;
};

export default Utilisateur(connection, DataTypes);
import { Model, DataTypes } from 'sequelize';
import connection from '../connection';

const Ville = (sequelize, DataTypes) => {
  class Ville extends Model {
    id;
    nom_ville;
  }
  Ville.init({
    nom_ville: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Ville',
    tableName: 'ville',
    timestamps: false,
  });
  return Ville;
};

export default Ville(connection, DataTypes);
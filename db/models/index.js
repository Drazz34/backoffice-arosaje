import Adresse from "./Adresse";
import Annonce from "./Annonce";
import CodePostal from "./CodePostal";
import Conseil from "./Conseil";
import Entretien from "./Entretien";
import Genre from "./Genre";
import Image from "./Image";
import Plante from "./Plante";
import Utilisateur from "./Utilisateur";
import Ville from "./Ville";
import RefreshToken from "./RefreshToken";
import Message from "./Message";

// Adresses associations
Adresse.hasMany(Utilisateur, { foreignKey: "adresse_id" });
Adresse.belongsTo(Ville, { foreignKey: "ville_id" });
Adresse.belongsTo(CodePostal, { foreignKey: "code_postal_id" });

// Annonces associations
Annonce.belongsTo(Plante, { foreignKey: "plante_id" });
Annonce.belongsTo(Utilisateur, { as: 'Proprietaire', foreignKey: "proprietaire_id" });
Annonce.belongsTo(Utilisateur, { as: 'Gardien', foreignKey: "gardien_id" });
Annonce.belongsTo(Image, { foreignKey: "image_id" });
Annonce.hasMany(Entretien, { foreignKey: "annonce_id" });

// CodePostal associations
CodePostal.hasMany(Adresse, { foreignKey: "code_postal_id" });

// Conseils associations
Conseil.belongsTo(Utilisateur, { as: 'Botaniste', foreignKey: "botaniste_id" });
Conseil.belongsTo(Plante, { foreignKey: "plante_id" });

// Entretiens associations
Entretien.belongsTo(Annonce, { foreignKey: "annonce_id" });
Entretien.belongsTo(Image, { foreignKey: "image_id" });

// Genres associations
Genre.hasMany(Utilisateur, { foreignKey: "genre_id" });

// Images associations
Image.hasMany(Entretien, { foreignKey: "image_id" });
Image.hasMany(Annonce, { foreignKey: "image_id" });
Image.hasMany(Utilisateur, { foreignKey: "image_id" });

// Plantes associations
Plante.hasMany(Annonce, { foreignKey: "plante_id" });
Plante.hasMany(Conseil, { foreignKey: "plante_id" });

// Utilisateurs associations
Utilisateur.belongsTo(Genre, { foreignKey: "genre_id" });
Utilisateur.belongsTo(Adresse, { foreignKey: "adresse_id" });
Utilisateur.belongsTo(Image, { foreignKey: "image_id" });
Utilisateur.hasMany(RefreshToken, { foreignKey: "utilisateur_id" });

// Villes associations
Ville.hasMany(Adresse, { foreignKey: "ville_id" });

// Refresh tokens associations
RefreshToken.belongsTo(Utilisateur, { foreignKey: "utilisateur_id" });

// Messages associations
Message.belongsTo(Utilisateur, { as: 'Expediteur', foreignKey: "expediteur_id" });
Message.belongsTo(Utilisateur, { as: 'Destinataire', foreignKey: "destinataire_id" });

export {
  Adresse,
  Annonce,
  CodePostal,
  Conseil,
  Entretien,
  Genre,
  Image,
  Plante,
  Utilisateur,
  Ville,
  RefreshToken
};

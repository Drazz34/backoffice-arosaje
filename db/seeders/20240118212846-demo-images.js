'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('image', [{
      // Remplacez ces valeurs par les valeurs réelles que vous souhaitez insérer
      chemin: '/img/users/amy-burns.png'
    }, {
      chemin: '/img/users/balazs-orban.png'
    }, {
      chemin: '/img/users/delba-de-oliveira.png'
    }, {
      chemin: '/img/users/emil-kowalski.png'
    }, {
      chemin: '/img/users/evil-rabbit.png'
    }, {
      chemin: '/img/users/guillermo-rauch.png'
    }, {
      chemin: '/img/users/hector-simpson.png'
    }, {
      chemin: '/img/users/jared-palmer.png'
    }, {
      chemin: '/img/users/lee-robinson.png'
    }, {
      chemin: '/img/users/michael-novotny.png'
    }, {
      chemin: '/img/users/steph-dietz.png'
    }, {
      chemin: '/img/users/steven-tey.png'
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     */
    await queryInterface.bulkDelete('image', null, {});
  }
};

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('plante', [{
      // Remplacez ces valeurs par les valeurs réelles que vous souhaitez insérer
      nom_plante: 'rosier',
      description: 'une plante qui pique !'
    }, {
      nom_plante: 'tournesol',
      description: ''
    }, {
      nom_plante: 'palmier',
      description: 'L\'arbre des vacances'
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     */
     await queryInterface.bulkDelete('plante', null, {});
     
  }
};

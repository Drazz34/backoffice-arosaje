'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * */
    await queryInterface.bulkInsert('adresse', [{
      rue: '349 Rue de la Cavalade',
      ville_id: 1,
      code_postal_id: 1
    },
  {
    rue: '39 bis avenue saint maurice de sauret',
    ville_id: 1,
    code_postal_id: 1
  }], {});

  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * */
    await queryInterface.bulkDelete('adresse', null, {});

  }
};

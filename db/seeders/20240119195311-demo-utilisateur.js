'use strict';
const bcrypt = require('bcrypt');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const hashedPassword = await bcrypt.hash('password', 10)
    await queryInterface.bulkInsert('utilisateur', [
      {
        pseudo: 'Toto',
        mail: 'toto@toto.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 1,
        adresse_id: 1
      },
      {
        pseudo: 'Botaniste',
        mail: 'botaniste@botaniste.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: true,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 2,
        image_id: 2,
        adresse_id: 2
      },
      {
        pseudo: 'Admin',
        mail: 'admin@admin.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: true,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 2,
        image_id: 3,
        adresse_id: 2
      },
      {
        pseudo: 'Botaniste Admin',
        mail: 'botaniste@admin.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: true,
        est_admin: true,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 2,
        image_id: 4,
        adresse_id: 2
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto1.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 5,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto2.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 6,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto3.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 7,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto4.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 8,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto5.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 9,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto6.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 10,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto7.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 11,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto8.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 12,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto9.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 1,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto10.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 2,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto11.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 3,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto12.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 4,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto13.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 5,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto14.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 6,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto15.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 7,
        adresse_id: 1
      },
      {
        pseudo: 'Toto',
        mail: 'toto@toto16.fr',
        mot_de_passe: hashedPassword,
        est_botaniste: false,
        est_admin: false,
        date_creation: new Date(),
        date_modif: null,
        genre_id: 1,
        image_id: 8,
        adresse_id: 1
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('utilisateur', null, {});
  }
};

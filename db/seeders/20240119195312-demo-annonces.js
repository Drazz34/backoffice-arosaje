'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * */
    await queryInterface.bulkInsert('annonce', [{
      date_debut: new Date(),
      date_fin: new Date(),
      periodicite_entretien: 3,
      date_creation: new Date(),
      image_id: 1,
      plante_id: 1,
      proprietaire_id: 1,
      gardien_id: 2
    },
    {
      date_debut: new Date(),
      date_fin: new Date(),
      periodicite_entretien: 5,
      date_creation: new Date(),
      image_id: 2,
      plante_id: 2,
      proprietaire_id: 2,
      gardien_id: 1
    }], {});

  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * */
    await queryInterface.bulkDelete('annonce', null, {});
     
  }
};

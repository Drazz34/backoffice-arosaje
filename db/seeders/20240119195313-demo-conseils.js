'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     */
    await queryInterface.bulkInsert('conseil', [{
      contenu: 'Bah faut l\'arroser',
      date_creation: new Date(),
      date_modif: new Date(),
      plante_id: 1
    },
    {
      contenu: 'Mettre à la lumière du jour',
      date_creation: new Date(),
      date_modif: new Date(),
      plante_id: 1,
      botaniste_id: 1
    }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     *  */
    await queryInterface.bulkDelete('conseil', null, {});
    
  }
};

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     */
    await queryInterface.bulkInsert('entretien', [{
      annonce_id: 1,
      image_id: 1,
      date_entretien: new Date()
    },
    {
      annonce_id: 1,
      image_id: 1,
      date_entretien: new Date()
    },
    {
      annonce_id: 1,
      image_id: 1,
      date_entretien: new Date()
    },
    {
      annonce_id: 1,
      image_id: 1,
      date_entretien: new Date()
    }], {});

  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
      */
    await queryInterface.bulkDelete('entretien', null, {});

  }
};

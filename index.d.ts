declare interface ApiResponse {
	success: boolean;
	message?: string;
	token?: string;
	refreshToken?: string;
}

declare interface AuthPayload {
	id: string;
	pseudo: string;
	mail: string;
	est_botaniste: boolean;
 	est_admin: boolean;
	iat: number;
	exp: number;
	openIdSub?: string;
}

declare interface ApiResponseType {
	body: object;
	httpStatusCode: { status: number };
}
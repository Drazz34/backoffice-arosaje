import {NextRequest, NextResponse} from 'next/server';
import {logout, verifyJwtAccessToken} from '@/app/lib/auth/auth';

// Add whatever paths you want to PROTECT here
// const authRoutes = ['*/app/*', '*/account/*', '*/api/*', '*/admin/*'];
const authRoutes = ['/api/*'];
const adminRoutes = ['/api/users'];
const publicRoutes = ['/api/auth/', '/api/genders'];

// Function to match the * wildcard character
function matchesWildcard(path: string, pattern: string): boolean {
	if (publicRoutes.some(publicRoute => path.startsWith(publicRoute))) {
		return false;
	}
	else if (pattern.endsWith('/*')) {
		const basePattern = pattern.slice(0, -2);
		return path.startsWith(basePattern);
	}
	return path === pattern;
}

export async function middleware(request: NextRequest) {
	// TODO: Gérer le middleware + suppression du token et redirection signin si le token n'est plus valide
	// Shortcut for our signin path redirect
	// Note: you must use absolute URLs for middleware redirects
	// const LOGIN = `${process.env.NEXT_PUBLIC_BASE_URL}/signin?redirect=${
	// 	request.nextUrl.pathname + request.nextUrl.search
	// }`;
	
	if (authRoutes.some(pattern => matchesWildcard(request.nextUrl.pathname, pattern))) {
		const accessToken = request.headers.get('Authorization')?.split('Bearer ')[1];
		// For API routes, we want to return unauthorized instead of redirecting to log in
		if (request.nextUrl.pathname.startsWith('/api')) {
			if (!accessToken) {
				const response: ApiResponse = {
					success: false,
					message: 'Non autorisé',
				};
				return NextResponse.json(response, { status: 401 });
			}
		}

		// If no token exists, redirect to log in
		if (!accessToken) {
			const response: ApiResponse = {
				success: false,
				message: 'Non autorisé',
			};
			return NextResponse.json(response, { status: 401 });
			// return NextResponse.redirect(LOGIN);
		}

		try {
			// Decode and verify JWT cookie
			const payload = await verifyJwtAccessToken(accessToken);

			console.log(payload)

			if (!payload) {
				return NextResponse.rewrite(new URL('/api/auth/refresh', request.nextUrl.href));
				// logout();
				// const response: ApiResponse = {
				// 	success: false,
				// 	message: 'Non autorisé',
				// };
				// return NextResponse.json(response, { status: 401 });
			}

			if (adminRoutes.some(adminRoute => request.nextUrl.pathname.startsWith(adminRoute))) {
				if (!payload.est_admin && request.nextUrl.searchParams.get('id') != payload.id) {
					const response: ApiResponse = {
						success: false,
						message: 'Non autorisé',
					};
					return NextResponse.json(response, { status: 401 });
				}
			}

			// If you have an admin role and path, secure it here
			// if (request.nextUrl.pathname.startsWith('/admin')) {
			// 	if (payload.role !== 'admin') {
			// 		return NextResponse.redirect(`${process.env.NEXT_PUBLIC_BASE_URL}/access-denied`);
			// 	}
			// }
		} catch (error) {
			// Delete token if authentication fails
			logout();
			const response: ApiResponse = {
				success: false,
				message: 'Une erreur est survenue',
			};
			return NextResponse.json(response, { status: 500 });
			// return NextResponse.redirect(LOGIN);
		}
	}

	// TODO: Gestion redirection au moment du signin coté backoffice
	// let redirectToApp = false;
	// //Redirect signin to app if already logged in
	// if (request.nextUrl.pathname === '/signin') {
	// 	const token = request.cookies.get('token');
	//
	// 	if (token) {
	// 		try {
	// 			const payload = await verifyJwtToken(token.value);
	//
	// 			if (payload) {
	// 				redirectToApp = true;
	// 			} else {
	// 				// Delete token
	// 				request.cookies.delete('token');
	// 			}
	// 		} catch (error) {
	// 			// Delete token
	// 			request.cookies.delete('token');
	// 		}
	// 	}
	// }
	//
	// if (redirectToApp) {
	// 	// Redirect to app dashboard
	// 	return NextResponse.redirect(`${process.env.NEXT_PUBLIC_BASE_URL}/app`);
	// } else {
	// 	// Return the original response unaltered
	// 	return NextResponse.next();
	// }

	// TODO: Bloqué l'accès à la doc api si l'environnement est différent de dev
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: ['/about/:path*', '/api/:path*'],
}